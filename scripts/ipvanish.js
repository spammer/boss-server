const _ = require('lodash');
const dns = require('dns');
const mongoose = require('mongoose');
const util = require('util');
const arequest = util.promisify(require('request'));
const cliProgress = require('cli-progress');
// process.env.NODE_ENV = 'production';
const config = require('../config');
const fs = require('fs');
const IP = require('../models/IP');
const VPN = require('../models/VPN');

const pbar = new cliProgress.Bar({
  format: 'progress [{bar}] {percentage}% | {value}/{total} | {serverDNS} ({serverIP})'
}, cliProgress.Presets.shades_classic);
const resolve4 = util.promisify(dns.resolve4);
const vpnDir = `${__dirname}/../VPN/ipvanish`;
const usVPNs = fs.readdirSync(vpnDir).filter(f => /-US-/.test(f));

const init = async () => {
  pbar.start(usVPNs.length, 0, {
    serverDNS: '',
    serverIP: 'checking IP score...'
  });
  await mongoose.connect(config.db, {
    useCreateIndex: true,
    useNewUrlParser: true
  });
  for (let i = 0; i < usVPNs.length; i++) {
    const fTxt = fs.readFileSync(`${vpnDir}/${usVPNs[i]}`, 'utf8');
    const serverDNS = /remote ([^\s]+)/gm.exec(fTxt)[1];
    let serverIP;

    try {
      [serverIP] = await resolve4(serverDNS);
    } catch (ex) {
      console.log(`${serverDNS} is fake DNS`);
      continue;
    }

    let oIP = await IP.findOne({ ip: serverIP });
    let oVPN = await VPN.findOne({ dns: serverDNS });

    if (!oVPN) {
      oVPN = new VPN({
        owner: 'IPVanish',
        dns: serverDNS
      });
      await oVPN.save();
    }
    if (oIP) {
      if (!oIP.VPN) {
        // uncomment the following only when _id is String instead of ObjectId
        // delete oIP._id;
        // delete oIP.vpn;
        // oIP.isNew = true;
        // await IP.findOneAndRemove({ ip: serverIP });
        oIP.VPN = oVPN._id;
        await oIP.save();
      }
      if (~_.get(oIP, 'quality.ipqualityscore.fraud_score', -1)) {
        console.log(`${serverDNS} (${serverIP}) already has score.`);
        continue;
      }
    } else {
      oIP = new IP({
        ip: serverIP,
        VPN: oVPN._id
      });
    }

    const { body } = await arequest(`https://www.ipqualityscore.com/api/json/ip/oy9Erur3IE8o9c3jla6GwXd3Y43dPv5z/${serverIP}?strictness=5`);
    let o;

    try {
      o = JSON.parse(body);
    } catch (ex) {
      console.log(`can't get score for ${serverIP}`);
      console.log(body);
      continue;
    }

    pbar.update(i + 1, { serverDNS, serverIP });
    _.assign(oIP.quality.ipqualityscore, _.pick(o, _.keys(oIP.quality.ipqualityscore)));
    _.assign(oIP.location, _.pick(o, _.keys(oIP.location)));
    await oIP.save();
  }
  pbar.stop();
  process.exit();
};

init();
