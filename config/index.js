const mongoose = require('mongoose');
const path = require('path');
const development = require('./env/development');
const test = require('./env/test');
const production = require('./env/production');

let pickedSettings;
const defaults = {
  dbConnect: async () => {
    pickedSettings.dbUrl = `mongodb://${process.env.MONGO_HOST || 'localhost'}:27017/${pickedSettings.dbName}`;
    await mongoose.connect(pickedSettings.dbUrl, {
      useCreateIndex: true,
      useNewUrlParser: true
    });
  },
  root: path.normalize(`${__dirname}/..`)
};

pickedSettings = {
  development: Object.assign({}, defaults, development),
  test: Object.assign({}, defaults, test),
  production: Object.assign({}, defaults, production)
}[process.env.NODE_ENV || 'development'];

module.exports = pickedSettings;
