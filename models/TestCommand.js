const mongoose = require('mongoose');

const TestCommand = new mongoose.Schema({
  afterAdClick: Boolean,
  clickSlot: String,
  connection: String, // can be: 'local', 'Shtip'
  fullScroll: Boolean,
  skipAdSave: {
    type: Boolean,
    default: true
  },
  skipRoam: Boolean
});

module.exports = mongoose.model('TestCommand', TestCommand);
