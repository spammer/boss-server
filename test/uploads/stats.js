const moment = require('moment');

exports.data = req => [{
  ip: '1',
  senders: [{
    date: moment(Number(req.query.to)).subtract(2, 'hours'),
    door: 'enter',
    sessionId: 'a1'
  }, {
    date: moment(Number(req.query.to)).subtract(2, 'hours').add(27, 'minutes'),
    clickSlot: '54322',
    door: '',
    sessionId: 'a1'
  }, {
    date: moment(Number(req.query.to)).subtract(1, 'hours'),
    door: 'exit',
    sessionId: 'a1'
  }, {
    date: moment(Number(req.query.to)).subtract(1, 'hours').add(25, 'minutes'),
    door: 'enter',
    sessionId: 'a9'
  }, {
    date: moment(Number(req.query.to)).subtract(1, 'hours').add(35, 'minutes'),
    door: 'exit',
    sessionId: 'a9'
  }]
}, {
  ip: '2',
  senders: [{
    date: moment(Number(req.query.to)).subtract(2, 'hours').add(6, 'minutes'),
    door: 'enter',
    sessionId: 'a2'
  }, {
    date: moment(Number(req.query.to)).subtract(2, 'hours').add(17, 'minutes'),
    door: '',
    clickSlot: '54322',
    sessionId: 'a2'
  }, {
    date: moment(Number(req.query.to)).subtract(2, 'hours').add(25, 'minutes'),
    door: 'enter',
    sessionId: 'a3'
  }, {
    date: moment(Number(req.query.to)).subtract(2, 'hours').add(29, 'minutes'),
    door: '',
    clickSlot: '54322',
    sessionId: 'a3'
  }, {
    date: moment(Number(req.query.to)).subtract(2, 'hours').add(45, 'minutes'),
    door: '',
    clickSlot: '54322',
    sessionId: 'a3'
  }, {
    date: moment(Number(req.query.to)).subtract(2, 'hours').add(55, 'minutes'),
    door: 'exit',
    sessionId: 'a3'
  }, {
    date: moment(Number(req.query.to)).subtract(1, 'hours').add(13, 'minutes'),
    door: 'enter',
    sessionId: 'a8'
  }, {
    date: moment(Number(req.query.to)).subtract(1, 'hours').add(30, 'minutes'),
    door: 'exit',
    sessionId: 'a8'
  }]
}, {
  ip: '3',
  senders: [{
    date: moment(Number(req.query.to)).subtract(2, 'hours').add(6, 'minutes'),
    door: 'enter',
    sessionId: 'a4'
  }, {
    date: moment(Number(req.query.to)).subtract(2, 'hours').add(35, 'minutes'),
    door: 'exit',
    sessionId: 'a4'
  }, {
    date: moment(Number(req.query.to)).subtract(1, 'hours').add(5, 'minutes'),
    door: 'enter',
    sessionId: 'a7'
  }, {
    date: moment(Number(req.query.to)).subtract(1, 'hours').add(10, 'minutes'),
    door: '',
    clickSlot: '54322',
    sessionId: 'a7'
  }],
  IP_quality: [{
    quality: {
      ipqualityscore: {
        fraud_score: 100
      }
    }
  }]
}, {
  ip: '4',
  senders: [{
    date: moment(Number(req.query.to)).subtract(2, 'hours').add(7, 'minutes'),
    door: 'enter',
    sessionId: 'a5'
  }, {
    date: moment(Number(req.query.to)).subtract(2, 'hours').add(15, 'minutes'),
    door: 'enter',
    sessionId: 'a6'
  }]
}];
