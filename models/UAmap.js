module.exports = {
  // https://github.com/ChromeDevTools/devtools-frontend/blob/master/front_end/emulated_devices/module.json
  _whitelist: [
    'iPhone 4',
    'iPhone 5/SE',
    'iPhone 6/7/8',
    'iPhone 6/7/8 Plus',
    'iPhone X',
    'Galaxy S III',
    'Galaxy S5',
    'Microsoft Lumia 950',
    'Nexus 6',
    'Pixel 2',
    'Pixel 2 XL',
    'Samsung Galaxy S8',
    'Samsung Galaxy S8+',
    'Samsung Galaxy S9+'
  ],
  _disperse: [{
    emulator: 'iPhone',
    patterns: [{
      ua: /.+Version\/7\.0.+/,
      models: /^iPhone 4/
    }, {
      ua: /(Version\/10\.0|Mobile\/14G60)/,
      models: /^iPhone [5-7S][A-Z]?( Plus)?/i
    }, {
      ua: /.+Version\/11\.0.+/,
      models: /^iPhone [8X][A-Z]?( Plus)?/i
    }, {
      ua: /.+Mobile\/15D60.+/,
      models: /^iPhone/
    }]
  }],
  'Blackberry PlayBook': [ // 1024x600;1
    'GALAXY Tab 3 7.0" Lite WiFi',
    'Fire HD 7 2017',
    'Kindle Fire'
  ],
  'iPhone 4': [ // 480x320;2
    'iPhone 4S',
  ],
  'iPhone 5/SE': [ // 568x320;2
    /^iPhone [5S][A-Z]?/,
    // 'iPhone 5',
    // 'iPhone 5C',
    // 'iPhone 5S',
    // 'iPhone SE'
  ],
  'iPhone 6/7/8': [ // 667x375;2
    'iPhone 6',
    'iPhone 6s',
    'iPhone 7',
    'iPhone 8'
  ],
  'iPhone 6/7/8 Plus': [ // 736x414;3
    'iPhone 6 Plus',
    'iPhone 6s Plus',
    'iPhone 7 Plus',
    'iPhone 8 Plus',
    'ZMAX Pro'
  ],
  'iPhone X': [ // 812x375;3
    'iPhone X'
  ],
  iPad: [ // 1024x768;2
    'iPad',
    'iPad Air',
    'iPad Air 2',
    'iPad 2',
    'iPad 3',
    'iPad 4',
    'iPad 5',
    'iPad 6'
  ],
  'iPad Mini': [ // 1024x768;2
    'iPad Mini',
    'iPad Mini 2',
    'iPad Mini 3',
    'iPad Mini 4',
    'iPad Pro 9.7',
    'GALAXY Tab A 8.0" WiFi',
    'GALAXY Tab A 9.7" WiFi',
    'GALAXY Tab E 9.6" WiFi',
    'GALAXY Tab S 8.4" WiFi',
    'SM-P350', // Galaxy TAB A 8.0 WI-FI SPEN
    'SM-T113', // Galaxy Tab E 7.0 4G
    'SM-T710',
    'SM-T713'
  ],
  'iPad Pro': [ // 1366x1024;2
    'iPad Pro 12.9',
    'iPad Pro 2 12.9'
  ],
  'Galaxy S III': [ // 640x360;2
    'C6742',
    'C6745', // Kyocera Hydro AIR
    'CUN-U29', // Huawei
    'Dash X2',
    'Desire 530', // HTC
    'Desire 550',
    'Desire 625',
    'GALAXY A3 (2016)',
    'GALAXY J3 (2016)',
    'H1611', // Huawei Ascend Mate
    'H892L', // Huawei Honor
    /^K\d{3}$/,
    // 'K373',
    // 'K425',
    // 'K428',
    // 'K450',
    // 'K540',
    // 'K550',
    /^LS\d{3}$/,
    // 'LS676',
    // 'LS775',
    // 'LS777',
    // 'LS990',
    // 'LS998',
    'L62VL',
    'L84VL',
    'Lumia 920',
    'Moto e',
    'MOTO E2',
    'Moto E4',
    'Moto E4 Plus',
    'Moto G',
    'M210',
    'M255', // LG
    'M257',
    'M322',
    'MP260',
    'MP450', // LG
    /^MS\d{3}$/,
    // 'MS210',
    // 'MS428',
    // 'MS550',
    // 'MS631',
    'SCH-S968C', // Samsung Galaxy S3
    /^SM-J[3-7]\d{2}/,
    // 'SM-J320A',
    // 'SM-J320AZ',
    // 'SM-J320V',
    // 'SM-J327A',
    // 'SM-J327P',
    // 'SM-J327T1',
    // 'SM-J327U',
    // 'SM-J327V',
    // 'SM-J327W',
    // 'SM-J530F',
    // 'SM-J700P',
    // 'SM-J700T',
    // 'SM-J700T1',
    // 'SM-J727A',
    // 'SM-J727AZ',
    // 'SM-J727P',
    // 'SM-J727T',
    // 'SM-J727T1',
    // 'SM-J727V',
    // 'SM-J737V',
    /^SM-S\d{3}/,
    // 'SM-S120VL',
    // 'SM-S327VL',
    // 'SM-S550TL',
    // 'SM-S727VL',
    // 'SM-S737TL',
    // 'SM-S907VL',
    /^SM-G55/,
    // 'SM-G550T',
    // 'SM-G550T1',
    // 'SM-G550T2',
    'SP200', // LG
    'SP320', // LG
    'TP260',
    'TP450',
    /^US\d{3}$/,
    // 'US215',
    // 'US375',
    // 'US610',
    'V930',
    'Xperia XA',
    'XT1700', // Motorola Moto E3
    'ZenFone 2E'
  ],
  'Galaxy S5': [ // 640x360;3
    'B2017G',
    'GALAXY Note 3',
    'GALAXY A5 (2017)',
    'GALAXY S4',
    'GALAXY S5',
    'H700', // LG
    'H740',
    'Moto G4',
    'Moto G5 Plus',
    'MOTO Z',
    'Moto Z2 Play',
    'MOTO x4',
    'Nexus 5',
    'P8 Lite (2017)', // Huawei
    'R9s', // Oppo
    'SGH-I337', // S4
    'SM-A520W',
    'SM-A530W',
    'XT1097',
    'XT1528',
    'XT1585',
    'Xperia Z2'
  ],
  'Microsoft Lumia 950': [ // 640x360;4
    'GALAXY Note 5',
    'GALAXY S6',
    'GALAXY S6 edge',
    'GALAXY S7',
    'GALAXY S7 edge',
    'M703',
    'H820',
    'H871', // LG G6
    /^SM-G[89][0-3]/, // Samsung Galaxy
    // 'SM-G860P',
    // 'SM-G870A',
    // 'SM-G890A',
    // 'SM-G891A',
    // 'SM-G903W', // Samsung Galaxy S5 Neo
    // 'SM-G920A', // Samsung Galaxy S6
    // 'SM-G920P',
    // 'SM-G920T',
    // 'SM-G920V',
    // 'SM-G920W8',
    // 'SM-G925A',
    // 'SM-G935A', // Samsung Galaxy S7 Edge
    'SM-G960F',
    'SM-G960U1',
    'SM-N920P',
    'SM-N920R7',
    'SM-N920V',
    'XT1254' // Motorola DROID Turbo
  ],
  'Nokia N9': [ // 854x480;1
    '3622A', // Coolpad Catalyst
    '3632A',
    '4060A', // Alcatel Ideal
    'A564C', // Alcatel OT
    'K7', // LG
    'L33L', // LG
    'L52VL',
    'L58VL', // LG
    'M153',
    'Optimus Zone 3', // LG
    'Phoenix 3',
    'SM-G360T',
    'SM-G360T1',
    'SM-G360V',
    'SM-G386T',
    /^SM-J1/, // Samsung Galaxy Express 3
    // 'SM-J100VPP',
    // 'SM-J120A',
    // 'SM-J120AZ',
    'VFD 501' // Vodafone Smart Turbo 7
  ],
  'Kindle Fire HDX': [ // 1280x800;2
    'Kindle Fire HD',
    'Kindle Fire HDX 7" WiFi',
    'Fire HD 6',
    'Fire 7"',
    'Fire HD 8 2016',
    'Fire HD 8 2017',
    'Fire HDX 8.9',
    'Fire HD 10',
    'GALAXY Note 10.1" 2014 Edition LTE',
    'GALAXY NotePRO 12.2" WiFi',
    'GALAXY Tab A 7.0" WiFi',
    'GALAXY Tab A 8.0" LTE',
    'GALAXY Tab A 10.1" WiFi (2016)',
    'GALAXY Tab 2 10.1" WiFi',
    'GALAXY Tab 3 8.0" WiFi',
    'GALAXY Tab 3 10.1" WiFi',
    'GALAXY Tab 4 7.0" WiFi',
    'GALAXY Tab 4 8.0" WiFi',
    'GALAXY Tab 4 10.1" WiFi',
    'GALAXY Tab S 10.5" LTE',
    'GALAXY TabPRO 12.2" WiFi',
    'GT-P7510', // Samsung Galaxy Tab 10.1
    'Iconia Tab 10', // Acer
    'IdeaTab A2109A', // Lenovo
    'MeMO Pad 7',
    /^RCT6/,
    // 'RCT6213W87M',
    // 'RCT6513W87',
    // 'RCT6603W47M7',
    // 'RCT6773W22B',
    // 'RCT6K03W13',
    /^SM-T3/, // Samsung Galaxy Tab A 8.0
    // 'SM-T357T',
    // 'SM-T377A',
    // 'SM-T377P',
    // 'SM-T377V',
    // 'SM-T378V',
    // 'SM-T380',
    'SM-T537V',
    'SM-T677A',
    /^SM-T8/,
    // 'SM-T807A',
    // 'SM-T807V',
    // 'SM-T817T',
    'TAB3 8', // Samsung Galaxy
    'UK495', // LG G Pad F 8.0
    'V495', // LG
    'V496',
    'Yoga Tab 3'
  ],
  'Nexus 6': [ // 732x412;3.5
    'H918',
    'Moto Z Play',
    'XT1565',
    'XT1575',
    'XT1650' // Motorola Moto Z
  ],
  'Nexus 7': [ // 960x600;2
    '810 4G Build', // LG G Pad
    'Iconia One 10', // Acer
    'SM-G530P',
    'SM-G530T', // Samsung Galaxy Grand Prime
    'SM-G530T1',
    'SM-P580' // Samsung Galaxy Tab A 10.1
  ],
  'Pixel 2': [ // 731x411;2.625
    'Pixel 2'
  ],
  'Pixel 2 XL': [ // 823x411;3.5
    'Pixel 2 XL'
  ],
  // Custom
  'Samsung Galaxy S8': [ // 740x360;3
    'EML-L09', // Huawei P20 L09
    'GALAXY S8'
  ],
  'Samsung Galaxy S8+': [ // 846x412;3.5
    'GALAXY Note 8',
    'GALAXY Note 8.0" WiFi',
    'GALAXY S8+'
  ],
  'Samsung Galaxy S9+': [ // 740x360;4
    'SM-G960U',
    'SM-G960W',
    'SM-G965U'
  ],
  'Samsung Galaxy Note 4': [ // 853x480;3
    'GALAXY Note 4'
  ],
  'iPad Pro 10.5': [ // 1112x834;2
    'iPad Pro 10.5'
  ]
};
