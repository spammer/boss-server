const mongoose = require('mongoose');
const TestCommand = require('./TestCommand');

const ad = new mongoose.Schema({
  postIdx: Number,
  pattern: String,
  size: String,
  code: String,
  image: Buffer,
  clickPos: String,
  clickSlotId: String,
  advertiser: String,

  pressDuration: {
    type: [Number],
    default: [50, 250]
  },
  // it's different than scroll.wait because combined
  // time for ad search is shorter than roaming time
  searchWait: {
    type: [Number],
    default: [100, 2000]
  },
  timeToSearch: {
    type: Number,
    default: 60000
  },
  emergencyClickTime: {
    type: Number,
    default: 5000
  }
});
const cookie = new mongoose.Schema({
  domain: String,
  expirationDate: Number,
  hostOnly: Boolean,
  httpOnly: Boolean,
  name: String,
  path: String,
  sameSite: String,
  secure: Boolean,
  session: Boolean,
  storeId: String,
  value: String
});
const link = new mongoose.Schema({
  clickedAt: Date, // recorded date after click
  selectors: String,
  time: Number, // ms to click after visit page while roaming
  url: String
});
const scroll = new mongoose.Schema({
  minPoints: {
    type: Number,
    default: 300
  },
  wait: {
    type: [Number],
    default: [100, 5000]
  }
});
const Command = new mongoose.Schema({
  boss: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Boss'
  },
  website: String,
  VPN: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'VPN'
  },
  IP: String,
  UserAgent: {
    type: String,
    required: true
  },
  emulator: {
    type: String,
    required: true
  },
  referer: String,
  cookies: [cookie],
  lang: String,
  posts: {
    type: [String],
    required: true,
    set: function onSetPosts(v) {
      if (!this.website) {
        const firstPost = new URL(v[0]);
        this.website = `${firstPost.protocol}//${firstPost.host}`;
      }
      return v;
    }
  },
  links: [link],
  visitAt: {
    type: Date,
    required: true
  },
  exitAt: {
    type: Date,
    required: true
  },
  lane: {
    type: Number,
    min: 0
  },
  scroll: {
    type: scroll,
    default: scroll
  },
  ad: {
    type: ad,
    default: ad
  },
  executed: {
    type: Boolean,
    default: false
  },
  prevSession: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Command'
  },
  sessionDuration: Number, // passed session time in seconds
  sessionStartedAt: Date,
  errorMsg: String,
  test: {
    type: TestCommand.schema,
    default: TestCommand.schema
  }
});

Command.statics.archiveProduction = async function archiveProduction() {
  const _ = require('lodash');
  const Boss = require('./Boss');
  const VPN = require('./VPN');
  const url = 'mongodb://localhost:27017';
  await mongoose.connect(`${url}/gd-production`, {
    useCreateIndex: true,
    useNewUrlParser: true
  });
  const cmds = await this.find({ executed: true }).populate('VPN');
  const bosses = await Boss.find();
  const vpns = _.uniqBy(cmds.map(c => c.VPN), '_id');
  await mongoose.disconnect();
  await mongoose.connect(`${url}/gd-archive`, {
    useCreateIndex: true,
    useNewUrlParser: true
  });
  const insert = async (where, what, label) => {
    let result;
    try {
      console.log(label, 'start inserting...');
      result = await where.insertMany(what, { ordered: false });
      console.log(`Inserted ${result.length} ${label} from ${what.length} executed`);
    } catch (ex) {
      if (ex.code === 11000) {
        console.log(`Inserted ${what.length - ex.result.result.writeErrors.length} ${label} from ${what.length} executed`);
      } else throw ex;
    }
  };
  await insert(this, cmds, 'commands');
  await insert(Boss, bosses, 'bosses');
  await insert(VPN, vpns, 'VPN-s');
  await mongoose.disconnect();
  require('child_process').execSync(`mongodump -d gd-archive -o ${__dirname}/../data/dump`);
  process.exit();
};

module.exports = mongoose.model('Command', Command);
