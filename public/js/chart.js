/* global _, $, moment, c3 */
const AD_SLOTS = {
  9000697720: 'ads respocnive 1',
  1477430928: 'ads responcive 2',
  4028279150: 'In-article',
  2954164127: 'ads responcive 3',
  8608051721: 'Ad2'
};
const REALTIME_INTERVAL = 1000;
const REALTIME_SPAN = [15, 'minutes'];
const START_MOMENT = moment.utc('2018-08-05').local();
const END_MOMENT = moment.utc('2018-08-08').local();
let _startDate = START_MOMENT;
let _endDate = END_MOMENT;
let _overview;
let _chart;
let _future;
let _dirtiness = $('#dirtyRange').val();
let _prevRow;
let _realtimeFn;
const $dtStart = $('#datetimeStart');
const $dtEnd = $('#datetimeEnd');
const $iptable = $('#iptable > table');
const $tooltip = $('#tooltip');
const getRandomStr = (info) => {
  console.error(`used random str: ${info}`);
  return Math.random().toString(36).substr(2, 10);
};
const organizeStats = (stats, separateDataCB, addTableRowCB) => {
  const sorted = [];
  const lanes = [];

  stats.forEach((stat) => {
    const groups = [];

    stat.senders.forEach((sender) => {
      const key = sender.door || (sender.clickSlot ? 'click' : 'enter');
      let g = _.find(groups, { sessionId: sender.sessionId });

      sender.date = moment(sender.date);
      sender.fraudScore = Number(_.get(stat, 'IP_quality[0].quality.ipqualityscore.fraud_score', ''));
      if (g) {
        g[key] = sender;
      } else {
        g = {};
        g[key] = sender;
        g.sessionId = sender.sessionId || getRandomStr(`${stat.ip} ${sender.date}`);
        groups.push(g);
      }
      if (addTableRowCB) {
        addTableRowCB(stat, sender);
      }
    });
    groups.map((g) => {
      if (!g.exit) {
        g.exit = g.click || g.enter;
      }
      if (!g.enter) {
        g.enter = g.exit;
      }
      g.IP = stat.ip;

      return g;
    });
    if (groups.length) {
      sorted.push(...groups);
    }
  });
  sorted.sort((a, b) => a.enter.date - b.enter.date);
  // let differentEnds = 0;
  sorted.forEach((o) => {
    let lIdx = _.findIndex(lanes, l => l.exit.date < o.enter.date);
    let y = 1;

    if (lIdx === -1) {
      lanes.push(o);
      lIdx = lanes.length - 1;
    } else {
      lanes[lIdx] = o;
    }
    y += 0.02 * lIdx;
    o.line = [[o.enter.date.valueOf(), y], [o.exit.date.valueOf(), y], null];
    if (o.click) {
      o.bar = [o.click.date.valueOf(), 2];
    }
    // if (o.line[0][0] !== o.line[1][0]) differentEnds++;
    separateDataCB(o);
  });
  // console.log('Start =/= End', differentEnds);
};
const updateAxes = () => {
  const min = _startDate.valueOf() - (1000 * 60 * 60 * 3);
  const max = _endDate.valueOf() + (1000 * 60 * 60 * 3);

  [_chart, _overview].forEach((c) => {
    const xaxis = c.getXAxes()[0];

    xaxis.options.min = min;
    xaxis.options.max = max;
    xaxis.options.panRange = [min, max];
    c.setupGrid();
    c.draw();
  });
  $iptable.find('.col-device').mouseenter((e) => {
    $tooltip.html($(e.target).data('tooltip')).css({
      top: e.pageY - 10,
      left: e.pageX + 20
    }).fadeIn(100);
  }).mouseleave(() => {
    $tooltip.hide();
  });
};
const addTableRow = $tbody => (stat, sender) => {
  $tbody.append([
    `<tr${sender.fraudScore ? ' class="bg-danger text-light"' : ''} onclick="rowSelect(this, ${sender.date.valueOf()})" id="statTime_${sender.date.valueOf()}">`,
    `<td class="col-ip">${stat.ip}<sup>${stat.senders.length}</sup></td>`,
    `<td class="col-click-slot">${AD_SLOTS[sender.clickSlot] || ''}</td>`,
    `<td class="col-device" data-tooltip="${sender.UserAgent}">${_.get(sender, 'UserDetected.device.type') || _.get(sender, 'UserDetected.os.name', '')}</td>`,
    `<td class="col-post">${sender.post}</td>`,
    `<td class="col-date">${sender.date.format('MM/DD HH:mm:ss')}</td>`,
    `<td class="col-score">${sender.fraudScore}</td>`,
    '</tr>'
  ].join(''));
};
const prepareData = (stats, addTableRowCB) => {
  const vc = {
    views: [],
    clicks: []
  };
  const data = Object.assign({
    server: _.cloneDeep(vc),
    dirty: Object.assign({
      server: _.cloneDeep(vc)
    }, _.cloneDeep(vc))
  }, _.cloneDeep(vc));
  // let differentUA = 0;
  organizeStats(stats, (o) => {
    const isDirty = o.enter.fraudScore > (100 - _dirtiness);

    if (isDirty) {
      data.dirty.views.push(...o.line);
      data.dirty.server.views.push(o);
      if (o.bar) {
        data.dirty.clicks.push(o.bar);
        data.dirty.server.clicks.push(o);
      }
    } else {
      data.views.push(...o.line);
      data.server.views.push(o);
      if (o.bar) {
        data.clicks.push(o.bar);
        data.server.clicks.push(o);
      }
    }
    // mystery: different UA per session
    // probable explanation: GoDaddy creates PHP IP cache when no DB or PHP changes detected
    // reason: there is no (enter) door on different UserAgents (best example: 172.58.175.213)
    //         there is (enter) only on first IP visited per website
    // TODO: To confirm this theory, research for PHP/Wordpress cache for different client requests
    // counterargument: why there are always similiar models per IP,
    //                  and not mixture of Android and iOS (see 96.89.191.57)?
    //                  Maybe could this be detected bot by GoDaddy?
    // interesting finding: I can use GoDaddy to better check my future IPs for dirtiness,
    //                      ipqualitycheck detects them as clean while GoDaddy as bots.
    //                      For example, ipqualitycheck doesn't detect 174.254.132.14 as bot
    // if (o.enter.UserAgent !== o.exit.UserAgent) {
    //   console.log([o.enter.door, o.exit.door],
    //     [o.enter.clickSlot, o.exit.clickSlot, !!o.click],
    //     [o.enter.sessionId, o.exit.sessionId, o.IP],
    //     [o.enter.UserAgent, o.exit.UserAgent],
    //     ++differentUA);
    // }
  }, addTableRowCB);

  const bars = {
    barWidth: 1000,
    fillColor: { colors: [{ opacity: 0.8 }, { opacity: 0.1 }] },
    highlightColor: '#007bff',
    show: true
  };
  const aData = [{
    label: `Clean IP (${data.views.length + data.clicks.length})`,
    data: data.views,
    serverData: data.server.views,
    lines: { show: true },
    points: { show: true }
  }, {
    label: `Dirty IP (${data.dirty.views.length + data.dirty.clicks.length})`,
    data: data.dirty.views,
    serverData: data.dirty.server.views,
    lines: { show: true },
    points: { show: true }
  }, {
    data: data.clicks,
    serverData: data.server.clicks,
    bars
  }, {
    data: data.dirty.clicks,
    serverData: data.dirty.server.clicks,
    bars
  }];

  return aData;
};
const getData = async () => {
  const d = await $.get([
    '/api/stats?total=1000',
    `&from=${_startDate.valueOf()}&to=${_endDate.valueOf()}`,
    '&dirtiness=100'
  ].join(''));
  const $tbody = $iptable.find('tbody').empty();
  const data = prepareData(d.stats, addTableRow($tbody));

  if (_chart) {
    _overview.setData(data);
    _chart.setData(data);
    if (!_realtimeFn) {
      updateAxes();
    }
  }
  $iptable.trigger('update');

  return data;
};

const getFutureData = async () => {
  const d = await $.get('/api/stats-test');
  const timerAxis = {
    y: {
      tick: {
        format: y => moment.utc(y).format('HH:mm:ss')
      }
    }
  };

  c3.generate({
    bindto: '#sessionDurations',
    // size: { width: 800, height: 350 },
    axis: timerAxis,
    legend: { show: false },
    data: {
      x: 'x',
      columns: [
        ['x', ...Array.from(new Array(d.durations.length), (v, i) => i + 1)],
        ['y', ...d.durations]
      ]
    },
    point: { r: 5 }
  });
  c3.generate({
    bindto: '#pagesPerSession',
    axis: timerAxis,
    data: {
      columns: d.pageData,
      type: 'bar',
      groups: [d.pageGroups]
    }
  });

  // return prepareData(d.stats);
  return [{
    label: `First time commands (${d.dataLen})`,
    data: d.data,
    minTime: d.min,
    maxTime: d.max,
    lines: { show: true },
    points: { show: true }
  }, {
    label: `Recurred commands (${d.dataLen2})`,
    data: d.data2,
    lines: { show: true },
    points: { show: true }
  }];
};
const updateFutureAxis = (data) => {
  const padding = moment(0).add(1, 'hour');
  const min = data[0].minTime - padding;
  const max = data[0].maxTime + padding;
  const xaxis = _future.getXAxes()[0];

  xaxis.options.min = min;
  xaxis.options.max = max;
  xaxis.options.panRange = [min, max];
  _future.setupGrid();
  _future.draw();
};
const detachFuture = (v) => {
  $('.detachable').css('bottom', 0);
  if (v) {
    $('.detachable').css('position', 'fixed');
  } else {
    $('.detachable').css('position', 'relative');
  }
};
const updateDirtiness = async (v) => {
  _dirtiness = v;
  $('#dirtyValue').text(v);
  await getData();
};
const switchRealtime = async (v) => {
  const xaxis = _chart.getXAxes()[0];

  if (v) {
    _realtimeFn = setInterval(async () => {
      _startDate = moment().subtract(...REALTIME_SPAN);
      _endDate = moment();
      await getData();
      xaxis.options.min = _startDate;
      xaxis.options.max = _endDate;
      xaxis.options.panRange = [_startDate, _endDate];
      _chart.setupGrid();
      _chart.draw();
    }, REALTIME_INTERVAL);
  } else {
    clearInterval(_realtimeFn);
    _realtimeFn = null;
    _startDate = START_MOMENT;
    _endDate = END_MOMENT;
    await getData();
  }
};
// hide eslint problem
if (!switchRealtime) { detachFuture(); updateDirtiness(); switchRealtime(); }
const rowSelect = (row, d) => {
  const o = _chart.getXAxes()[0].options;

  o.min = d - (1000 * 20);
  o.max = d + (1000 * 20);
  _chart.setupGrid();
  _chart.draw();
  if (_prevRow) {
    $(_prevRow).removeClass('table-active');
  }
  $(row).toggleClass('table-active');
  _prevRow = row;
};
const onPlotSelected = chart => (e, ranges) => {
  // do the zooming
  $.each(chart.getXAxes(), (_, axis) => {
    axis.options.min = ranges.xaxis.from;
    axis.options.max = ranges.xaxis.to;
  });
  chart.setupGrid();
  chart.draw();
  chart.clearSelection();
};

$(async () => {
  const data = await getData();
  const futureData = await getFutureData();
  const options = {
    colors: ['#007bff', '#dc3545', '#007bff', '#dc3545', '#AFD8F8', '#CB4B4B', '#4DA74D', '#9440ED'],
    grid: {
      hoverable: true,
      clickable: true
    },
    xaxis: {
      mode: 'time',
      timezone: 'browser',
      minTickSize: [1, 'second']
    },
    yaxis: {
      panRange: [0, 3],
      zoomRange: [0, 3],
      ticks: [[1, 'views'], [2, 'clicks']]
    },
    legend: {
      backgroundColor: 'transparent'
    }
  };

  _overview = $.plot('#overview', data, Object.assign({}, options, {
    selection: {
      color: '#4da5f4',
      mode: 'x'
    },
    legend: {
      show: false
    }
  }));
  _chart = $.plot('#chart', data, Object.assign({}, options, {
    pan: {
      interactive: true
    },
    zoom: {
      interactive: true
    }
  }));
  _future = $.plot('#future', futureData, Object.assign({}, options, {
    pan: {
      interactive: true
    },
    zoom: {
      interactive: true
    }
  }));
  updateFutureAxis(futureData);
  $('#overview').on('plotselected', (e, ranges) => {
    _chart.setSelection(ranges);
  });
  $('#chart')
    .on('plotselected', onPlotSelected(_chart))
    .on('plotclick', (e, pos, item) => {
      if (item) {
        const date = item.datapoint[0];
        const $tr = $(`#statTime_${date}`);
        const $container = $tr.closest('tbody');

        $container.scrollTop(($tr.offset().top - $container.offset().top)
          + ($container.scrollTop() - 100));
        rowSelect($tr[0], date);
      }
    });
  $('#future').on('plotselected', onPlotSelected(_future));
  $('#chart,#future').on('plothover', (e, pos, item) => {
    if (item) {
      const d = e.currentTarget.id === 'future'
        ? futureData[0].data
        : data[0].data;
      const lineIdx = _.findIndex(d, a =>
        a && a[0] === item.datapoint[0] && a[1] === item.datapoint[1]);

      if (lineIdx === -1) return;
      const time = d[lineIdx - 1]
        ? d[lineIdx][0] - d[lineIdx - 1][0]
        : d[lineIdx + 1][0] - d[lineIdx][0];

      $tooltip.html(moment.utc(time).format('HH:mm:ss'))
        .css({
          top: pos.pageY - 10,
          left: pos.pageX + 20
        }).fadeIn(100);
    } else {
      $tooltip.hide();
    }
  });
  $iptable.tablesorter();
  $dtStart.datetimepicker({
    defaultDate: _startDate,
    maxDate: _endDate,
    inline: true,
    sideBySide: true
  });
  $dtEnd.datetimepicker({
    defaultDate: _endDate,
    minDate: _startDate,
    inline: true,
    sideBySide: true
  });
  $dtStart.on('change.datetimepicker', async (e) => {
    $('#datetimeEnd').datetimepicker('minDate', _startDate = e.date);
  });
  $dtEnd.on('change.datetimepicker', async (e) => {
    $('#datetimeStart').datetimepicker('maxDate', _endDate = e.date);
  });
  updateAxes();
  detachFuture($('#cbDetach').is(':checked'));
});
