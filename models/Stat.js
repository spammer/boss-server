const mongoose = require('mongoose');

module.exports = mongoose.model('Stat', new mongoose.Schema({
  ip: {
    type: String,
    unique: true
  },
  isProxy: Boolean,
  senders: [{
    clickSlot: String,
    date: {
      type: Date,
      default: Date.now
    },
    door: String,
    dpi: String,
    post: String,
    screenSize: String,
    sessionId: {
      type: String,
      required: true,
      default: Math.random().toString(36).substr(2, 5)
    },
    UserAgent: {
      type: String,
      required: true
    },
    UserDetected: Object
  }]
}));
