const fs = require('fs');
const DNS = require('dns');
const resolve4 = require('util').promisify(DNS.resolve4);
const { dbConnect } = require('../../config');
const countries = require('../../data/countries');
const VPN = require('../../models/VPN');
const { zipConfig, testZip } = require('../vpn-configs');

exports.regions = {
  mel: ['AU', 'Melbourne'],
  syd: ['AU', 'Sydney'],
  gig: ['BR', 'Rio de Janeiro'],
  gru: ['BR', 'Sao Paulo'],
  yul: ['CA', 'Montreal'],
  yyz: ['CA', 'Toronto'],
  yvr: ['CA', 'Vancouver'],
  prg: ['CZ', 'Prague'],
  cdg: ['FR', 'Paris'],
  fra: ['DE', 'Frankfurt'],
  muc: ['DE', 'Munich'],
  hkg: ['HK', 'Hong Kong'],
  tlv: ['IL', 'Tel Aviv'],
  mxp: ['IT', 'Milan'],
  hnd: ['JP', 'Tokyo'],
  kul: ['MY', 'Kuala Lumpur'],
  mex: ['MX', 'Mexico City'],
  ams: ['NL', 'Amsterdam'],
  osl: ['NO', 'Oslo'],
  mnl: ['PH', 'Manila'],
  waw: ['PL', 'Warsaw'],
  sin: ['SG', 'Singapore'],
  icn: ['KR', 'Seoul'],
  mad: ['ES', 'Madrid'],
  arn: ['SE', 'Stockholm'],
  zrh: ['CH', 'Zurich'],
  tav: ['TR', 'Istanbul'],
  mse: ['UK', 'Canterbury'],
  edi: ['UK', 'Livingston'],
  icy: ['UK', 'London'],
  ihr: ['UK', 'Maidenhead'],
  man: ['UK', 'Manchester'],
  rug: ['UK', 'Rugby'],
  atl: ['US', 'Atlanta'],
  buf: ['US', 'Buffalo'],
  dfw: ['US', 'Dallas'],
  iad: ['US', 'Washington D.C'],
  jfk: ['US', 'New York'],
  las: ['US', 'Las Vegas'],
  lax: ['US', 'Los Angeles'],
  mia: ['US', 'Miami'],
  mke: ['US', 'Milwaukee'],
  ord: ['US', 'Chicago'],
  phx: ['US', 'Phoenix'],
  sea: ['US', 'Seattle'],
  sfo: ['US', 'San Francisco']
};

exports.fillStrongVPN = async () => {
  const regCodes = Object.keys(exports.regions);
  const masterOvpn = `${__dirname}/../../VPN/StrongVPN/master.ovpn`;
  const config = fs.readFileSync(masterOvpn, 'utf8');
  let dns, saveMsg, vpn;

  DNS.setServers(['8.8.8.8']);
  for (const c of regCodes) {
    for (let i = 1; i <= 500; i++) {
      try {
        dns = `str-${c}${i}.strongconnectivity.com`;
        vpn = await VPN.findOne({ dns });
        if (!vpn) {
          vpn = new VPN({
            owner: 'StrongVPN',
            dns,
            ip: await resolve4(dns)
          });
          if (!vpn.ip) throw new Error('unreachable');
          fs.writeFileSync(masterOvpn, config.replace(/remote str-[^.]+/, `remote str-${c + i}`));
          vpn.config = await zipConfig('StrongVPN', 'master.ovpn');
          saveMsg = 'inserted';
        } else saveMsg = 'overwritten';
        vpn.country = countries[exports.regions[c][0]];
        vpn.region = exports.regions[c][1];
        await vpn.save();
        console.log(`${dns} ${saveMsg}`);
      } catch (ex) {
        if (ex.errno !== 'ENOTFOUND' && ex.message !== 'unreachable') throw ex;
      }
    }
  }
};

exports.run = async () => {
  await dbConnect();
  await exports.fillStrongVPN();
  const test = await VPN.findOne({ owner: 'StrongVPN', dns: /lax\d+/ });
  testZip(test.config);
  process.exit();
};

exports.run();
