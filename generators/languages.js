const _ = require('lodash');

exports.generateLanguages = (languages, sessions) => {
  const sortFn = (a, b) => a.items - b.items;
  const items = languages.filter(l => l.items).sort(sortFn);
  const itemized = languages.filter(l => l.rate).map((l) => {
    l.items = sessions.length * l.rate;
    return l;
  }).sort(sortFn);
  const extra = [];

  items.concat(itemized).forEach((l) => {
    const group = _.shuffle(sessions.filter(s => !s.lang && s.VPN.country === l.country))
      .slice(0, l.items);

    group.forEach(s => s.lang = l.code);
    if (l.items > group.length) {
      extra.push(..._.times(l.items - group.length, _.constant(l.code)));
    }
  });

  const restSessions = _.shuffle(sessions.filter(s => !s.lang));
console.log(restSessions.length, extra.length);
console.log('sessions', _.fromPairs(_.sortBy(_.toPairs(_.countBy(sessions, 'VPN.country'))), 1).reverse());
console.log('extraSessions', _.countBy(extra, 'VPN.country'));
console.log('restSessions', _.countBy(restSessions, 'VPN.country'));
  extra.forEach((code, i) => restSessions[i].lang = code);
  console.log(sessions.filter(s => !s.lang).map(s => s.VPN.country));
};

exports.test = async () => {
  const Boss = require('../models/Boss');
  const boss = new Boss({
    sessions: {
      total: 1000
    }
  });
  const { sessions } = require('./sessions').generateSessions(boss);

  await require('../config').dbConnect();
  await require('./vpn-configs').generateVPNs(boss, sessions);
  this.generateLanguages(boss.languages, sessions);
  // console.log(_.groupBy(boss.languages, 'country'));
  console.log(1 - boss.languages.reduce((a, b) => a + b.rate, 0));

  process.exit();
};

exports.test();
