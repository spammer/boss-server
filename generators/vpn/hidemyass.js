const countries = require('../../data/countries');
const { includedCountries, run } = require('../vpn-configs');

// require('../vpn-configs').findMissingOvpn('hidemyass/UDP', 'hidemyass/TCP',
//   ovpn => new RegExp(`^${ovpn.match(/(.+)\.(UD|TC)P\.ovpn$/)[1]}\\.`));

const filteredCountries = Object.entries(countries)
  .filter(([code]) => ~includedCountries.indexOf(code))
  .map(([, country]) => country.replace(' ', '')).join('|')
  .replace('UnitedKingdom', 'UK')
  .replace('UnitedStates', 'USA');
const getRegionFn = (ovpn, config) => {
  const serverDNS = /remote ([^\s]+)/gm.exec(config)[1];
  return {
    serverDNS,
    region: /(.+)\.hma\.rocks/.exec(serverDNS)[1].split('.').reverse().join('.').replace(/^gb/, 'uk')
  };
};

run('HideMyAss', filteredCountries, getRegionFn);
