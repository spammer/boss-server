mongo gd-production --eval 'db.getCollection("ips").count()'
read -p "Copy DB?" answer
case ${answer:0:1} in
	y|Y )
		mongodump -d gd-production -c ips --archive | mongorestore --drop --nsFrom 'gd-production.ips' --nsTo 'gd.ips' --archive
	;;
esac