const detector = new (require('node-device-detector'))();

// process.env.NODE_ENV = 'production';
require('mongoose').connect(require('../config').db, {
  useCreateIndex: true,
  useNewUrlParser: true
});
require('../models/Stat').find().exec(async (err, docs) => {
  for (const stat of docs) {
    for (const sender of stat.senders) {
      sender.UserDetected = detector.detect(sender.UserAgent);
    }
    stat.markModified('senders');
    await stat.save();
  }
  process.exit();
});
