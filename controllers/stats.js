const detector = new (require('node-device-detector'))();
const { body, header, validationResult } = require('express-validator/check');
const Stat = require('../models/Stat');
const Command = require('../models/Command');

exports.statsFilterAggregate = (from, to) => [{
  $project: {
    ip: 1,
    isProxy: 1,
    senders: {
      $filter: {
        input: '$senders',
        as: 's',
        cond: {
          $and: [{
            $gte: ['$$s.date', new Date(Number(from) || 0)]
          }, {
            $lte: ['$$s.date', new Date(Number(to) || Date.now())]
          }]
        }
      }
    }
  }
}, {
  $match: {
    'senders.0': {
      $exists: true
    }
  }
}];

exports.crawlersMatch = {
  $match: {
    'senders.UserAgent': {
      $not: /bot|crawler|spider|\(compatible;|Indexer|archiver|Apache|facebook|wordpress|google|admantx/i
    }
  }
};

exports.index = async (req, res) => {
  const a = this.statsFilterAggregate(req.query.from, req.query.to);

  if (!req.query.allowBots) {
    a.push(this.crawlersMatch);
  }
  a.push({
    $lookup: {
      from: 'ips',
      localField: 'ip',
      foreignField: 'ip',
      as: 'IP_quality'
    }
  }, {
    $match: {
      $or: [{
        IP_quality: [],
      }, {
        'IP_quality.0.quality.ipqualityscore.fraud_score': {
          $lte: Number(req.query.dirtiness) || 0
        }
      }]
    }
  }, {
    $limit: Number(req.query.total) || 50
  });
  res.json({
    stats: await Stat.aggregate(a)
  });
};

exports.isME = (req, res, next) => {
  if (~['::ffff:95.180.177.70', '::1', '::ffff:127.0.0.1', '::ffff:92.53.34.229'].indexOf(req.ip)) {
    return next();
  }
  res.redirect('/login');
};

exports.validatePOST = [
  body('ip').isIP(),
  body('sessionId').isAlphanumeric(),
  header('User-Agent').not().isEmpty().withMessage('No User-Agent header!')
];

exports.displayErrors = (req, res, next) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    const msg = errors.mapped();
    res.status(422).send(msg);
    if (process.env.NODE_ENV !== 'test') console.error(msg);
    return;
  }
  next();
};

exports.create = async (req, res) => {
  const o = req.body;
  const sender = {
    clickSlot: o.clickSlot,
    door: o.door,
    dpi: o.dpi,
    post: o.post,
    screenSize: o.screenSize,
    sessionId: o.sessionId,
    UserAgent: req.get('User-Agent')
  };
  let stat = await Stat.findOne({
    ip: o.ip
  });

  sender.UserDetected = detector.detect(sender.UserAgent);
  if (!stat) {
    stat = new Stat({
      ip: o.ip,
      isProxy: o.isProxy,
      senders: [sender]
    });
  } else {
    stat.senders.push(sender);
  }
  stat.markModified('senders');
  await stat.save();

  res.sendStatus(200);
  return stat;
};

exports.ips = async (req, res) => {
  res.json({
    ips: await require('../models/IP').find()
  });
};

exports.testIndex = async (req, res) => {
  const data = [], data2 = [], durations = [], stackedXLimit = 5;
  const pageGroups = Array.from(new Array(stackedXLimit), (v, i) => `page${i + 1}`);
  const pageData = [...pageGroups.map(v => [v])];
  const cmds = await Command.find();
  let min = Infinity, max = 0, pageIdx = 0, dataLen = 0, dataLen2 = 0;

  for (const f of cmds) {
    const y = 1 + (f.lane * 0.02);
    const visitAt = f.visitAt.getTime();
    const exitAt = f.exitAt.getTime();
    const line = [[visitAt, y], [exitAt, y], null];

    if (min > visitAt) min = visitAt;
    if (max < exitAt) max = exitAt;
    durations.push(f.exitAt - f.visitAt);
    if (f.prevSession) {
      data2.push(...line);
      dataLen2++;
    } else {
      data.push(...line);
      dataLen++;
    }
    if (f.links && f.links.length === stackedXLimit && pageIdx < stackedXLimit) {
      f.links.forEach((v, i) => pageData[i].push(v.time));
      pageIdx++;
    }
  }
  durations.sort((a, b) => a - b);

  res.json({
    // stats: require('../test/uploads/stats').data(req)
    data,
    dataLen,
    data2,
    dataLen2,
    durations,
    pageGroups,
    pageData,
    min,
    max
  });
};
