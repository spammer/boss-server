const _ = require('lodash');
const cheerio = require('cheerio');
const { get } = require('socks5');

exports.hardcoded = {
  expensive: [
    // 'http://beautifull.life/how-to-find-insurers-doctors-and-lawyers-after-a-car-accident/',
    // 'http://beautifull.life/how-best-to-compensate-as-a-mesothelioma-patient/'
  ]
};

exports.generatePosts = async (c) => {
  const body = await get(c.website);
  const $ = cheerio.load(body);
  const all = _.map($('.entry-title a'), 'attribs.href'); // Array.from(document.querySelectorAll('.entry-title a')).map(a=>a.href)
  const combinedPercent = c.posts.expensive + c.posts.mixed;
  const expensive = c.ctr ? Array.from({ length: c.posts.expensive * c.sessions.total },
    () => [_.sample(this.hardcoded.expensive)]) : [];
  const mixed = c.ctr ? Array.from({ length: c.posts.mixed * c.sessions.total },
    () => [_.sample(all), _.sample(this.hardcoded.expensive)]) : [];
  const rest = Array.from({
    length: c.sessions.total - (c.ctr ? (combinedPercent * c.sessions.total) : 0)
  }, () => [_.sample(all)]);
  const totalClicks = Math.ceil(c.ctr * c.sessions.total);
  const combined = expensive.concat(mixed);

  if (c.ctr > combinedPercent) {
    throw new Error('posts.expensive + posts.mixed constants must be higher than CTR!');
  }

  const clicks = _.shuffle(combined).slice(0, totalClicks);
  const views = _.shuffle(combined.concat(rest)).slice(0, c.sessions.total - totalClicks);
  const limit = (duration, cls) => cls
    .slice(-Math.ceil(duration / c.sessions.minTime.beforeAdClick));
  let pc = 0;
  let pv = 0;

  return {
    nextClick: duration => limit(duration, clicks[pc++]),
    nextView: duration => limit(duration, views[pv++])
  };
};

// (async () => {
//   const moment = require('moment');
//   const c = {
//     website: 'http://alwaysbehealthy.online',
//     ctr: 0,
//     sessions: {
//       total: 50,
//       avgSessionTime: moment(0).add(71, 'seconds'),
//       bounceRate: 0.8783,
//       minTime: {
//         beforeAdClick: moment(0).add(45, 'seconds'),
//         beforeLastClick: moment(0).add(0, 'seconds'),
//         betweenClicks: moment(0).add(10, 'seconds'),
//         betweenSessions: moment(0).add(8, 'seconds'),
//         bouncedSession: moment(0).add(2, 'seconds'),
//         interactive: moment(0).add(3, 'seconds')
//       }
//     },
//     since: moment(),
//     duration: moment(0).add(24, 'hours'),
//     posts: {
//       expensive: 0.3,
//       minClickTime: moment(0).add(1, 'minute'),
//       mixed: 0.2
//     }
//   };
//   const { sessions } = require('./sessions').generateSessions(c);
//   const posts = await exports.generatePosts(c);

//   sessions.map((s) => {
//     if (s.click) s.posts = posts.nextClick(s.duration);
//     else s.posts = posts.nextView(s.duration);
//     return s;
//   }).forEach((s, i) => console.log(i, s.posts.length, s.click, s.posts[0]));
//   process.exit();
// })();
