const _ = require('lodash');
const { execSync } = require('child_process');
const fs = require('fs');
const mongoose = require('mongoose');

const JQuerySelector = new mongoose.Schema({
  selector: {
    type: String,
    required: true
  },
  length: {
    type: Number,
    min: 0
  },
  comment: String
});

const AdPattern = new mongoose.Schema({
  name: String,
  advertiser: {
    type: String,
    required: true
  },
  adSize: {
    type: String,
    required: true
  },
  image: {
    type: Buffer,
    required: true
  },
  code: {
    type: Buffer,
    // required: true
  },
  selPatterns: [JQuerySelector],
  clicks: {
    positions: String,
    sum: {
      type: Number,
      default: 0
    },
    future: String,
    past: String
  }
}, { timestamps: true });

AdPattern.methods.updateClick = function updateClick(click) {
  const aPos = (this.clicks.positions || '').split(',');

  if (aPos.indexOf(click) === -1) {
    throw new Error(`${click} is not contained in AdPattern clicks.positions: "${this.clicks.positions}"`);
  }

  const aPast = this.clicks.past ? this.clicks.past.split(',') : [];

  this.clicks.future = _.without(aPos, ...aPast, click);
  aPast.push(click);
  this.clicks.past = aPast.join(',');
};

AdPattern.statics.mapImagesToDB = async function mapImagesToDB() {
  await require('../config').dbConnect();
  await Promise.all((await this.find()).map((a) => {
    const path = `${__dirname}/../../98111116/adtypes/${a.name}`;

    a.advertiser = 'Google Ads';
    if (fs.existsSync(`${path}/code.html`)) a.code = fs.readFileSync(`${path}/code.html`);
    a.image = fs.readFileSync(`${path}/bg.png`);

    return a.save();
  }));
  process.exit();
};

AdPattern.statics.createPatterns = async function createPatterns() {
  const db = require('../config').dbName;

  await this.deleteMany();
  execSync(`mongorestore --db ${db} -c adpatterns --drop data/dump/${db}/adpatterns.bson --quiet`);
  return (await this.find()).map((p) => {
    p = p.toObject();
    delete p.createdAt;
    delete p.updatedAt;
    p.selPatterns.forEach(sp => delete sp._id);
    return p;
  });
};

AdPattern.statics.testCreatePatterns = async () => {
  await require('../config').dbConnect();
  await module.exports.createPatterns();
  process.exit();
};

module.exports = mongoose.model('AdPattern', AdPattern);

// mongoose.model('AdPattern').testCreatePatterns();
