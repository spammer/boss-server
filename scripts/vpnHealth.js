const cliProgress = require('cli-progress');
const fs = require('fs');
const request = require('request-promise');
const { dbConnect } = require('../config');
const VPN = require('../models/VPN');
// const { zipConfig } = require('../generators/vpn-configs');

const certDir = `${__dirname}/../../98111116/certificates`;
const pBar = new cliProgress.Bar({
  format: 'Checking VPN connections [{bar}] {percentage}% | {dns} | {value}/{total}'
}, cliProgress.Presets.shades_classic);
const sleep = require('util').promisify(setTimeout);

const logStream = fs.createWriteStream('tmp/strongVPN.log');

exports.agentOptions = {
  ca: fs.readFileSync(`${certDir}/ca.crt`),
  cert: fs.readFileSync(`${certDir}/cert.crt`),
  key: fs.readFileSync(`${certDir}/cert.key`),
  rejectUnauthorized: false
};

exports.startVPN = async (lane, vpn) => {
  try {
    await request({
      method: 'POST',
      url: `https://94.100.96.66:8789/start?lane=${lane}`,
      agentOptions: this.agentOptions,
      json: {
        lane,
        zip: Buffer.from(vpn.config).toString('base64') // await zipConfig('test', `${vpn.dns}.udp.ovpn`)
      }
    });
    vpn.lastErrorMsg = undefined;
  } catch (ex) {
    logStream.write(`\n[${new Date().toISOString()}] ${lane}: ${ex}`);
    vpn.lastErrorMsg = ex.message;
  }
  await vpn.save();
};

exports.stopVPN = async (lane) => {
  try {
    await request({
      url: `https://94.100.96.66:8789/end?lane=${lane}`,
      agentOptions: this.agentOptions
    });
  } catch (ex) {
    if (/No such container/.test(ex.message)) return;
    logStream.write(`\nstopping vpn: ${lane}: ${ex}`);
  }
};

exports.checkVPN = async (lane) => {
  let vpn;

  while (this.vpns.length) {
    vpn = this.vpns.pop();
    await exports.startVPN(lane, vpn);
    pBar.update(++this.counter, { dns: vpn.dns });
    await exports.stopVPN(lane);
    // NordVPN: 7.5 minutes
    await sleep(+require('moment')(0).add(45, 'seconds'));
  }
};

exports.endAll = async () => {
  await request({
    url: 'https://94.100.96.66:8789/endall',
    agentOptions: this.agentOptions
  });
  process.exit();
};

exports.checkHealth = async () => {
  this.vpns = await VPN.find({
    // dns: {
    //   $in: [
    //     // "za10.nordvpn.com" // waiting too long
    //     // "za11.nordvpn.com",
    //     // "za12.nordvpn.com",
    //     // "za13.nordvpn.com",
    //     // "za14.nordvpn.com",
    //     // "za15.nordvpn.com",
    //     // "za16.nordvpn.com",
    //     // "za17.nordvpn.com",
    //     // "za18.nordvpn.com",
    //     // "za19.nordvpn.com",
    //     // "za20.nordvpn.com"
    //     "us1600.nordvpn.com"
    //   ]
    // },
    // ip: {
    //   $in: [
    //     '61.14.210.229'
    //   ]
    // },
    // lastErrorMsg: null,
    owner: {
      $in: ['ExpressVPN', 'HideMyAss']
    }
  });
  this.counter = 0;
  pBar.start(this.vpns.length, 0, { dns: '' });
  await Promise.all(Array.from(new Array(4), (v, i) => exports.checkVPN(i + 1)));
  pBar.stop();
  await exports.endAll();
};

exports.run = async () => {
  await dbConnect();
  await exports.checkHealth();
  logStream.end();
  process.exit();
};

exports.run();
