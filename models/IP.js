const mongoose = require('mongoose');

module.exports = mongoose.model('IP', new mongoose.Schema({
  ip: {
    type: String,
    unique: true
  },
  VPN: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'VPN'
  },
  quality: {
    ipqualityscore: {
      proxy: Boolean,
      is_crawler: Boolean,
      vpn: Boolean,
      tor: Boolean,
      recent_abuse: Boolean,
      mobile: Boolean,
      fraud_score: Number
    }
  },
  location: {
    ISP: String,
    city: String,
    region: String
  }
}).pre('find', function preFind() {
  this.populate('VPN');
}));
