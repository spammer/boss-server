#!/bin/bash
cp node_modules/bootstrap/dist/js/bootstrap.min.* public/js/lib
cp node_modules/jquery/dist/jquery.min.* public/js/lib
cp node_modules/moment/min/moment.min.js public/js/lib/moment.min.js
cp -r node_modules/tempusdominus-bootstrap-4/build/* public
cp -r node_modules/jquery.flot/jquery.flot.* public/js/lib
cp node_modules/lodash/lodash.min.js public/js/lib
cp node_modules/tablesorter/dist/js/jquery.tablesorter.min.js public/js/lib
cp node_modules/d3/dist/d3.min.js public/js/lib
cp node_modules/c3/c3.min.css public/css/lib
cp node_modules/c3/c3.min.js public/js/lib
npm rebuild node-sass
