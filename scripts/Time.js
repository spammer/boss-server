const moment = require('moment');

module.exports = {
  hms: ms => moment.utc(moment.duration(ms).asMilliseconds()).format('H[h] m[m] s[s]')
};
