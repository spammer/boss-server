const moment = require('moment');
const mongoose = require('mongoose');
const TestCommand = require('./TestCommand');

const minTime = new mongoose.Schema({
  beforeAdClick: {
    type: Number,
    default: moment(0).add(45, 'seconds')
  },
  beforeLastClick: {
    type: Number,
    default: moment(0).add(0, 'seconds')
  },
  betweenClicks: {
    type: Number,
    default: moment(0).add(10, 'seconds'),
  },
  betweenSessions: {
    type: Number,
    default: moment(0).add(8, 'seconds'),
  },
  bouncedSession: {
    type: Number,
    default: moment(0).add(2, 'seconds')
  },
  interactive: {
    type: Number,
    default: moment(0).add(3, 'seconds')
  }
});
const pageSpawn = new mongoose.Schema({
  maxTime: Number,
  maxPages: Number
});
const pageSelectors = new mongoose.Schema({
  exceptLast: {
    type: String,
    default: '.td-post-prev-post a,.td-post-next-post a'
  },
  last: {
    type: String,
    default: 'a.entry-crumb'
  }
});
const sessions = new mongoose.Schema({
  total: {
    type: Number,
    default: +process.env.npm_config_total || 1134
  },
  avgSessionTime: {
    type: Number,
    default: moment(0).add(71, 'seconds')
  },
  bounceRate: {
    type: Number,
    default: 0.85
  },
  minTime: {
    type: minTime,
    default: minTime
  },
  pagesPerSession: {
    type: Number,
    default: 1.22
  },
  pageSpawn: {
    type: [pageSpawn],
    default: [{
      maxTime: moment(0).add(7, 'seconds'),
      maxPages: 1
    }, {
      // min = minTime.betweenClicks * 2 + 1s
      maxTime: moment(0).add(21, 'seconds'),
      maxPages: 1
    }, {
      maxTime: moment(0).add(2, 'minutes'),
      maxPages: 2
    }, {
      maxTime: moment(0).add(6, 'minutes'),
      maxPages: 3
    }]
  },
  pageSelectors: {
    type: pageSelectors,
    default: pageSelectors
  }
});
const language = new mongoose.Schema({
  country: String,
  code: String,
  rate: Number,
  items: Number
});
const posts = new mongoose.Schema({
  expensive: {
    type: Number,
    default: 0.3
  },
  mixed: {
    type: Number,
    default: 0.2
  }
});
const vpnProvider = new mongoose.Schema({
  owner: {
    type: String,
    required: true
  },
  allowedLanes: [Number],
  clicksOnly: Boolean
});
const Boss = new mongoose.Schema({
  website: {
    type: String,
    default: 'http://alwaysbehealthy.online', // avoid trailing '/' slash
    // default: 'http://foodnews.online',
    // default: 'http://healthynews24.online',
    // default: 'http://beautifull.life',
  },
  lanes: {
    type: Number,
    default: 4
  },
  ctr: {
    type: Number,
    default: 0
  },
  sessions: {
    type: sessions,
    default: sessions
  },
  languages: {
    type: [language],
    // http://www.webapps-online.com/online-tools/languages-and-locales
    default: [{
      country: 'United States',
      code: 'en-us',
      rate: 0.9164
    }, {
      country: 'United Kingdom',
      code: 'en-gb',
      rate: 0.0367
    }, {
      country: 'Canada',
      code: 'en-ca',
      rate: 0.0313
    }, {
      country: 'Australia',
      code: 'en-au',
      rate: 0.0084
    }, {
      country: 'Canada',
      code: 'fr-ca',
      rate: 0.0011
    }, {
      country: 'United States',
      code: 'es-us',
      rate: 0.0009
    }, {
      country: 'Mexico',
      code: 'es-xl',
      rate: 0.0004
    }, {
      country: 'Brazil',
      code: 'es-xl',
      rate: 0.0004
    }, {
      country: 'Argentina',
      code: 'es-xl',
      rate: 0.0004
    }, {
      country: 'United States',
      code: 'en',
      rate: 0.0006
    }, {
      country: 'France',
      code: 'fr-fr',
      rate: 0.0007
    }, {
      country: 'China',
      code: 'zh-cn',
      rate: 0.0008
    }, {
      country: 'Germany',
      code: 'de-de',
      rate: 0.0007
    }, {
      country: 'Spain',
      code: 'es-es',
      rate: 0.0006
    }, {
      country: 'South Korea',
      code: 'ko-kr',
      rate: 0.0006
    }]
  },
  since: {
    type: Date,
    default: Date.now
  },
  duration: {
    type: Number,
    default: moment(0).add(+process.env.npm_config_hours || 24, 'hours')
  },
  posts: {
    type: posts,
    default: posts
  },
  vpnProviders: {
    type: [vpnProvider],
    default: [{
      owner: 'HideMyAss',
      allowedLanes: [1, 2, 5]
    }, {
      owner: 'ExpressVPN',
      allowedLanes: [3, 4]
    }]
  },
  returningUsersRate: {
    type: Number,
    default: 0.4
  },
  test: {
    type: TestCommand.schema,
    default: TestCommand.schema
  }
});

module.exports = mongoose.model('Boss', Boss);
