const mongoose = require('mongoose');
const { body } = require('express-validator/check');
const AdPattern = require('../models/AdPattern');

exports.index = async (req, res) => {
  const resObj = {};
  const a = [];

  if (req.query.filter) {
    const contains = {
      $regex: req.query.filter.replace(/[.*+?^${}()|[\]\\]/g, '\\$&')
    };
    const searchFields = [
      'name',
      'advertiser',
      'selPatterns.selector',
      'selPatterns.comment',
      'adSize'
    ];

    a.push({
      $match: {
        $or: searchFields.map((f) => {
          const o = {};
          o[f] = contains;
          return o;
        })
      }
    });
  }
  if (req.query.sort && req.query.order) {
    const sort = {};

    sort[req.query.sort] = req.query.order === 'asc' ? 1 : -1;
    a.push({
      $sort: sort
    });
  }
  resObj.items = await AdPattern.aggregate(a.concat([{
    $count: 'total'
  }]));
  resObj.total = resObj.items.length ? resObj.items[0].total : 0;

  if (req.query.page > 0 && req.query.pageSize > 0) {
    a.push({ $skip: req.query.page * req.query.pageSize });
  }
  a.push({ $limit: Number(req.query.pageSize) || resObj.total });
  resObj.items = (await AdPattern.aggregate(a)).map((item) => { // TODO: try use $addFields instead
    item.image = `data:image/png;base64,${item.image.toString('base64')}`;
    return item;
  });
  res.json(resObj);
};

exports.getHtml = async (req, res) => {
  const adp = await AdPattern.findById(req.query.id);
  const code = (adp || {}).code || '';

  res.status(200).send(code.toString('utf8'));
};

exports.validatePOST = [
  body('advertiser').not().isEmpty()
];

exports.create = async (req, res) => {
  try {
    let adp;

    if (req.params.id) {
      adp = await AdPattern.findById(req.params.id);
    } else {
      adp = new AdPattern();
    }
    Object.assign(adp, req.body);
    await adp.save();
    res.status(200).send(adp.id);
  } catch (e) {
    res.status(422).send(e.message);
    if (process.env.NODE_ENV !== 'test') console.error(e.message);
  }
};

exports.remove = async (req, res) => {
  try {
    const { ids } = req.query;

    if (!ids) throw new Error('IDs are empty!');
    await AdPattern.deleteMany({
      _id: {
        $in: ids.split(',').map(id => mongoose.Types.ObjectId(id))
      }
    });
    res.status(200).end();
  } catch (e) {
    res.status(422).send(e.message);
    if (process.env.NODE_ENV !== 'test') console.error(e.message);
  }
};
