mongo gd-production --eval 'db.getCollection("stats").count()'
read -p "Copy DB?" answer
case ${answer:0:1} in
	y|Y )
		mongodump -d gd-production -c stats --archive | mongorestore --drop --nsFrom 'gd-production.stats' --nsTo 'gd.stats' --archive
	;;
esac