const countries = require('../../data/countries');
const { includedCountries, run } = require('../vpn-configs');

const countriesPairs = Object.entries(countries)
  .filter(([code]) => ~includedCountries.indexOf(code))
  .map(([code, country]) => [code, country
    .replace('Czech Republic', 'Czech')
    .replace('United Kingdom', 'UK')
    .replace('United States', 'USA')
  ]);
const filteredCountries = countriesPairs
  .map(([, country]) => `TorGuard.${country.replace(' ', '.')}`).join('|');
const getRegionFn = (ovpn, config) => {
  const country = /^TorGuard\.(.+)\.ovpn$/.exec(ovpn)[1]
    .replace('.', ' ').replace(/UK.+/, 'UK').replace(/USA.+/, 'USA');
  const foundPair = countriesPairs.find(([, v]) => ~country.indexOf(v));
  const region = foundPair ? foundPair[0] : '';
  return {
    serverDNS: /remote ([^ ]+)/.exec(config)[1],
    region
  };
};

run('TorGuard', filteredCountries, getRegionFn);
