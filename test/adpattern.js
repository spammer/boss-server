const request = require('supertest');
const t = require('tap');
const { app } = require('../app');
const AdPattern = require('../models/AdPattern');

let patterns;

t.test('prepare db for testing', async () => {
  patterns = await AdPattern.createPatterns();
});

t.test('reject invalid access', (t) => {
  request(app)
    .get('/api/boss/ad-variants')
    .expect(500)
    .catch(t.fail)
    .finally(t.end);
});

t.test('refuse creating without advertiser', (t) => {
  delete patterns[0]._id;
  delete patterns[0].advertiser;
  request(app)
    .post('/api/boss/ad-variant')
    .set('Origin', 'http://localhost')
    .send(patterns[0])
    .expect(422)
    .catch(t.fail)
    .finally(t.end);
});

t.test('create with empty pattern', (t) => {
  delete patterns[1]._id;
  delete patterns[1].name;
  request(app)
    .post('/api/boss/ad-variant')
    .set('Origin', 'http://localhost')
    .send(patterns[1])
    .expect(200)
    .then(async () => {
      const ap = await AdPattern.find({ name: undefined });

      t.ok(ap);
      t.ok(!ap.name);
    })
    .catch(t.fail)
    .finally(t.end);
});

t.test('update ad variant', (t) => {
  const p = patterns[2];

  p.name += '_test';
  p.selPatterns = [{
    selector: '#test.test_ad',
    length: 2
  }];
  request(app)
    .post(`/api/boss/ad-variant/${p._id}`)
    .set('Origin', 'http://localhost')
    .send(p)
    .expect(200, p._id.toString())
    .then(async () => {
      const ap = await AdPattern.findById(p._id);

      t.equal(ap.selPatterns[0].selector, '#test.test_ad');
    })
    .catch(t.fail)
    .finally(t.end);
});

t.test('refuse delete patterns with invalid IDs', (t) => {
  request(app)
    .delete('/api/boss/ad-variants')
    .set('Origin', 'http://localhost')
    .send({})
    .expect(422, /IDs are empty/)
    .catch(t.fail)
    .finally(t.end);
});

t.test('delete ad variant', (t) => {
  request(app)
    .delete(`/api/boss/ad-variants?ids=${[patterns[3]._id, patterns[4]._id]}`)
    .set('Origin', 'http://localhost')
    .expect(200)
    .then(async () => {
      const ap = await AdPattern.findById(patterns[4]._id);

      t.ok(!ap);
    })
    .catch(t.fail)
    .finally(t.end);
});

t.test('get Ad Variants', (t) => {
  process.env.ORIGIN = 'http://some.domain:4200';
  request(app)
    .get('/api/boss/ad-variants')
    .set('Origin', 'http://some.domain:4200')
    .expect(200)
    .then((res) => {
      const { items, total } = res.body;

      t.equal(total, 16);
      t.ok(items[5].name);
      t.ok(/^data:image\/png;base64,/.test(items[6].image));
      t.ok(items[7].image.length > 10000);
    })
    .catch(t.fail)
    .finally(t.end);
});

t.tearDown(process.exit);
