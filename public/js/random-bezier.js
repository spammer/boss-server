/* global window, _ */

/* ========================================================================

 X0, Y0, Xf, Yf [, O]

 + Required parameters:
 - X0 and Y0     The initial coordinates of the mouse movement
 - Xf and Yf     The final coordinates of the mouse movement

 + Optional parameters:
 - O             Options string, see remarks below (default: blank)

 It is possible to specify multiple (case insensitive) options:

 # "Tx" (where x is a positive number)
   > The time of the mouse movement, in miliseconds
   > Defaults to 200 if not present
 # "Px" or "Py-z" (where x, y and z are positive numbers)
   > "Px" uses exactly 'x' control points
   > "Py-z" uses a random number of points (from 'y' to 'z', inclusive)
   > Specifying 1 anywhere will be replaced by 2 instead
   > Specifying a number greater than 19 anywhere will be replaced by 19
   > Defaults to "P2-5"
 # "S" means sorted control points
 # "OTx" (where x is a number) means Offset Top
 # "OBx" (where x is a number) means Offset Bottom
 # "OLx" (where x is a number) means Offset Left
 # "ORx" (where x is a number) means Offset Right
   > These offsets, specified in pixels, are actually boundaries that
     apply to the [X0,Y0,Xf,Yf] rectangle, making it wider or narrower
   > It is possible to use multiple offsets at the same time
   > When not specified, an offset defaults to 100
     - This means that, if none are specified, the random Bézier control
       points will be generated within a box that is wider by 100 pixels
       in all directions, and the trajectory will never go beyond that

======================================================================== */

window.RandomBezier = (param) => {
  let M = /T(\d+)/i.exec(param.O);
  const Time = M ? Number(M[1]) : 200;
  let N = 2;

  M = /P(\d+)(-(\d+))?/i.exec(param.O);
  if (M) {
    if (M[1] < 2) {
      N = 2;
    } else if (M[1] > 19) {
      N = 19;
    } else {
      N = Number(M[1]);
    }
    if (M[3]) {
      if (M[3] < 2) {
        M = 2;
      } else if (M[3] > 19) {
        M = 19;
      } else {
        M = Number(M[3]);
      }
    } else {
      M = null;
    }
  } else {
    M = 5;
  }
  if (M) {
    N = _.random(N, M);
  }

  const isSorted = / S/i.test(param.O);
  const sortFN = (a, b) => a - b;
  const OfT = (M = /OT(-?\d+)/i.exec(param.O)) ? Number(M[1]) : 100;
  const OfB = (M = /OB(-?\d+)/i.exec(param.O)) ? Number(M[1]) : 100;
  const OfL = (M = /OL(-?\d+)/i.exec(param.O)) ? Number(M[1]) : 100;
  const OfR = (M = /OR(-?\d+)/i.exec(param.O)) ? Number(M[1]) : 100;
  const X = [];
  const Y = [];
  let sX;
  let sY;
  let bX;
  let bY;

  if (param.X0 < param.Xf) {
    sX = param.X0 - OfL;
    bX = param.Xf + OfR;
  } else {
    sX = param.Xf - OfL;
    bX = param.X0 + OfR;
  }
  if (param.Y0 < param.Yf) {
    sY = param.Y0 - OfT;
    bY = param.Yf + OfB;
  } else {
    sY = param.Yf - OfT;
    bY = param.Y0 + OfB;
  }
  N--;
  for (let i = 0; i < N - 1; i++) {
    X[i] = _.random(sX, bX);
    Y[i] = _.random(sY, bY);
  }
  X.unshift(param.X0);
  Y.unshift(param.Y0);
  if (isSorted) {
    X.sort(sortFN);
    Y.sort(sortFN);
  }
  X[N] = param.Xf;
  Y[N] = param.Yf;
  param.onStart(X[0], Y[0]);

  const I = new Date().getTime();
  const E = I + Time;
  const whileFn = () => {
    const tickCount = new Date().getTime();
    const T = (tickCount - I) / Time;
    const U = 1 - T;
    let x = 0;
    let y = 0;

    if (tickCount >= E) {
      param.onEnd(X[N], Y[N]);
      return;
    }
    for (let i = 1; i <= N + 1; i++) {
      const Idx = i - 1;
      const D = N - Idx;
      let F1 = 1;
      let F2 = 1;
      let F3 = 1;

      for (let j = 1; j <= Idx; j++) {
        F2 *= j;
        F1 *= j;
      }
      for (let j = 1; j <= D; j++) {
        F3 *= j;
        F1 *= j + Idx;
      }
      M = (F1 / (F2 * F3)) * ((T + 0.000001) ** Idx) * ((U - 0.000001) ** D);
      x += M * X[Idx];
      y += M * Y[Idx];
    }
    param.onMove(x, y);
    setTimeout(whileFn, 1);
  };
  whileFn();

  return N + 1;
};
