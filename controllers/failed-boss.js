const _ = require('lodash');
const config = require('../config');
const fs = require('fs');
const multer = require('multer');

const adtypesDir = `${config.root}${process.env.NODE_ENV === 'test' ? '/test' : ''}/adtypes`;

exports.index = (req, res) => {
  const a = [];

  fs.readdirSync(adtypesDir).forEach((folder) => {
    const folderDir = `${adtypesDir}/${folder}`;
    const clicksPath = `${folderDir}/clicks.png`;
    const o = {};

    o[folder] = {
      img: fs.readFileSync(`${folderDir}/bg.png`).toString('base64'),
      code: fs.readFileSync(`${folderDir}/code.html`, 'utf-8')
    };
    if (fs.existsSync(clicksPath)) {
      o[folder].clicks = fs.readFileSync(clicksPath).toString('base64');
    }

    a.push(o);
  });

  res.json({
    variants: a
  });
};

exports.validateFiles = (req, res, next) => {
  const files = [];

  multer({
    dest: adtypesDir,
    fileFilter: (req, file, cb) => {
      files.push(file);
      console.log(req);
      cb(new Error('Invalid uploaded files!'));
    }
  }).fields([
    { name: 'code', maxCount: 1 },
    { name: 'bg', maxCount: 1 }
  ])(req, res, (err) => {
    console.log(files);
    if (files.length !== 2) {
      res.status(422).send('Missing uploaded files!');
      return;
    }
    if (err) {
      res.status(422).send(err);
      return;
    }

    next();
  });
};

exports.createAd = (req, res) => {
  const unknown = `${adtypesDir}/unknown`;
  const getFile = (name) => {
    fs.writeFileSync(`${unknown}/${name}.png`, _.find(req.files, { name }));
  };

  fs.mkdirSync(unknown);
  getFile('bg');
  getFile('code');

  res.status(200).send('File uploaded!');
};

exports.isBot = (req, res, next) => {
  if (~['::ffff:127.0.0.1', '::1'].indexOf(req.ip)) {
    return next();
  }
  res.redirect('/login');
};
