process.env.NODE_ENV = 'production';
require('mongoose').connect(require('../config').db, {
  useCreateIndex: true,
  useNewUrlParser: true
});
const Command = require('../models/Command');
const Stat = require('../models/Stat');

const getDetails = async (ua) => {
  let c = await Stat.findOne({ 'senders.UserAgent': ua });

  console.log(`UserAgent: ${ua}`);
  console.log(`model: ${c.senders[0].UserDetected.device.model}`);
  c = await Command.findOne({ UserAgent: ua });
  console.log(`emulator: ${c.emulator}`);
  process.exit();
};

getDetails('Mozilla/5.0 (Linux; Android 5.0.2; LG-V496 Build/LRX22G) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.83 Safari/537.36');
