const _ = require('lodash');

exports.generateSessions = (c) => {
  const addLine = (s, l, r, y, idx) => {
    const lineEnd = _.random(l + s.spaceMin + s.min, r - s.spaceMin);
    const maxLength = lineEnd - s.max - s.spaceMin;
    const lineStartMin = maxLength < (l + s.spaceMin)
      ? l + s.spaceMin
      : maxLength;
    const lineStart = _.random(lineStartMin, lineEnd - s.min);

    return {
      visitAt: lineStart,
      exitAt: lineEnd,
      duration: lineEnd - lineStart,
      lane: y,
      laneIdx: idx
    };
  };
  let nextLane = 0;
  let lastSpaceMin = c.sessions[0].spaceMin;
  const nextLaneSettings = () => {
    const s = c.sessions[nextLane++ % c.sessions.length];

    if (!s.spaceMin) {
      s.spaceMin = lastSpaceMin;
    }
    lastSpaceMin = s.spaceMin;

    return s;
  };
  const clicks = [];
  const lines = [];
  const until = c.since + c.duration;
  let l;
  let y = 1;
  let laneIdx = 0;
  let session = nextLaneSettings();
  let r = c.since + session.min + (2 * session.spaceMin);
  let restLaneTime;
  let restSessions;

  for (let i = 0; i < c.totalSessions; i++) {
    l = r;
    restSessions = c.totalSessions - i;
    restLaneTime = until - r;
    r += restLaneTime < (restSessions * session.max)
      ? session.max
      : Math.round(restLaneTime / restSessions);

    if (r > until) {
      y++;
      laneIdx = 0;
      if (c.maxLanes && y > c.maxLanes) {
        break;
      }
      r = (r - l) + c.since;
      l = c.since;
      session = nextLaneSettings();
    }

    const line = addLine(session, l, r, y, laneIdx++);

    if (line.duration > c.posts.minClickTime) {
      clicks.push(i);
    }
    lines.push(line);
  }
  _.shuffle(clicks).slice(0, Math.ceil(c.ctr * c.totalSessions)).forEach((i) => {
    lines[i].click = true;
  });

  return {
    laneSum: y,
    sessions: lines
  };
};

exports.generateLinks = (c, sessions) => {
  const totalSessionTime = c.totalSessions * c.links.avgSessionTime;
  const totalSessionsWithLinks = c.totalSessions - (c.links.bounceRate * c.totalSessions);
  const avgSessionWithLinksTime = Math.round(totalSessionTime / totalSessionsWithLinks);
  const getLinkTime = () => _.random(c.links.minTimeBetweenClicks, avgSessionWithLinksTime);
  let sumSessionTime = 0;
  const sessionsWithLinks = _.shuffle(sessions
    .filter(s => !s.click && s.duration > c.links.minTimeBetweenClicks))
    .slice(0, totalSessionsWithLinks);
  const totalDurationWithLinks = sessionsWithLinks
    .reduce((acc, b) => acc + (b.duration - c.links.clickScrollTime), 0);
  const getRestDuration = s => s.duration - _.last(s.links).time;
  let t, maxT, rest = 0, j = 0;

  if (totalDurationWithLinks < totalSessionTime) {
    console.error('Sum of bounced sessions duration is not fit for the given avgSessionTime and bounceRate');
    console.error(`Sum of bounced sessions duration = ${Math.round(totalDurationWithLinks / 1000)}s`);
    console.error(`Max average session time = ${Math.round(avgSessionWithLinksTime / 1000)}s`);
    console.error(`Expected total session time = ${totalSessionTime / 1000}s`);
    console.error(`Only ${Math.round((totalDurationWithLinks / totalSessionTime) * 100)}% filled`);
    console.error('Try generate again. If this is common, try changing constants.\n');
    return true;
  }
  sessionsWithLinks.forEach((s) => {
    t = getLinkTime();
    maxT = s.duration - c.links.clickScrollTime;
    if (t > maxT) t = maxT;
    s.links = [{ time: t }];
    sumSessionTime += t;
  });
  if (sumSessionTime > totalSessionTime) {
    console.error('This should not happen, run again');
    return true;
  }

  for (const s of sessionsWithLinks) {
    let i = 1, sT;

    t = s.links[0].time;
    do {
      sT = getLinkTime();
      if (t + sT > s.duration - c.links.clickScrollTime) break;
      t += sT;
      s.links.push({ time: t });
    } while (_.random(1) && ++i < c.links.maxLinksPerSeesion);
    sumSessionTime += t - s.links[0].time;
    if (sumSessionTime > totalSessionTime) {
      sumSessionTime -= t - s.links[0].time;
      rest = totalSessionTime - sumSessionTime;
      s.links = [s.links[0]];
      break;
    }
  }
  if (!rest) rest = totalSessionTime - sumSessionTime;
  sessionsWithLinks.sort((a, b) => {
    if (!a || !b) console.log(a, b);
    return getRestDuration(b) - getRestDuration(a);
  });
  while (rest) {
    const s = sessionsWithLinks[j++];
    let r = getRestDuration(s) - c.links.clickScrollTime;

    if (rest - r < 0) {
      r = rest;
      rest = 0;
    } else rest -= r;
    _.last(s.links).time += r;
  }
};

(() => {
  const moment = require('moment');
  const c = {
    totalSessions: 3500,
    ctr: 0.1,
    // these numbers are optimised for 9 lanes
    sessions: [{
      min: moment(0).add(20, 'seconds'),
      max: moment(0).add(1.5, 'minutes'),
      spaceMin: moment(0).add(10, 'seconds')
    }, {
      min: moment(0).add(30, 'seconds'),
      max: moment(0).add(2, 'minutes')
    }, {
      min: moment(0).add(45, 'seconds'),
      max: moment(0).add(5, 'minutes')
    }, {
      min: moment(0).add(1, 'minute'),
      max: moment(0).add(4, 'minutes'),
      spaceMin: moment(0).add(20, 'seconds')
    }, {
      min: moment(0).add(50, 'seconds'),
      max: moment(0).add(8, 'minutes')
    }, {
      min: moment(0).add(1, 'minute'),
      max: moment(0).add(20, 'minutes')
    }, {
      min: moment(0).add(5, 'minutes'),
      max: moment(0).add(40, 'minutes')
    }, {
      min: moment(0).add(10, 'minute'),
      max: moment(0).add(45, 'minutes'),
      spaceMin: moment(0).add(1, 'minute')
    }],
    since: moment(),
    duration: moment(0).add(24, 'hours'),
    links: {
      avgSessionTime: moment(0).add(71, 'seconds'),
      bounceRate: 0.5,
      clickScrollTime: 10000,
      maxLinksPerSeesion: 3,
      minTimeBetweenClicks: 45000
    },
    posts: {
      expensive: 0.3,
      minClickTime: moment(0).add(1, 'minute'),
      mixed: 0.2
    }
  };
  const testCount = 100;
  let sumTime = 0, sumBounced = 0, ceilBroken = 0, pass = 0;
  const checkSessionFn = (s) => {
    if (s.links) {
      const t = _.last(s.links).time;

      if (s.duration - t < c.links.clickScrollTime) ceilBroken++;
      sumTime += t;
    } else sumBounced++;
  };

  for (let i = 0; i < testCount; i++) {
    const { sessions } = exports.generateSessions(c);
    const isError = exports.generateLinks(c, sessions);

    if (isError) continue;
    sumTime = sumBounced = ceilBroken = 0;
    sessions.forEach(checkSessionFn);
    console.assert(sumTime / c.totalSessions === +c.links.avgSessionTime,
      'avgSessionTime is not ok');
    console.assert(c.links.bounceRate * c.totalSessions === sumBounced,
      'bounceRate is not ok');
    console.assert(!ceilBroken,
      `clickScrollTime is not respected: ${ceilBroken} times`);
    pass++;
  }
  console.log(`Passed ${pass} of ${testCount} test.`);
})();
