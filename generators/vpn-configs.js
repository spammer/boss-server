const _ = require('lodash');
const Zip = require('adm-zip');
const fs = require('fs');
const { promisify } = require('util');
const glob = promisify(require('glob'));
const path = require('path');
const resolve4 = promisify(require('dns').resolve4);
const countries = require('../data/countries');
const VPN = require('../models/VPN');

// $sort: { 'quality.ipqualityscore.fraud_score': -1 } // TODO: add this somehow to generator
exports.getVPNs = async (boss, owner) => {
  const vpns = await VPN.find({
    owner,
    lastErrorMsg: null
  }).lean();

  if (vpns.length) return vpns;
  console.error(`Run ${owner}.fill() method first`);
  // await Promise.all(vpnOwners
  //   .map(o => require(`./vpn/${o.toLowerCase()}`)
  //    .fill())); // eslint-disable-line import/no-dynamic-require
  // return getList();
};

// DEPRECATED
// exports.setRecurred = (returningUsersRate, vpns) => {
//   const totalRecurrings = Math.round(returningUsersRate * vpns.length);
//   const map = {}, result = [];
//   let temp, r = 0;
//   vpns.forEach((vpn) => {
//     if (!map[vpn.country]) map[vpn.country] = [];
//     map[vpn.country].push(vpn);
//   });
//   _.forEach(map, (lv) => {
//     temp = _.shuffle(lv);
//     for (let i = 0; i < Math.round(lv.length * returningUsersRate); i++) {
//       if (++r > totalRecurrings) break;
//       temp[i].canRecur = 1;
//     }
//     result.push(...temp);
//   });
//   return _.shuffle(result);
// };
// exports.testSetRecurred = async () => {
//   await require('../config').dbConnect();
//   const vpns = await VPN.find({ owner: 'StrongVPN' });
//   const returningUsersRate = 0.4;
//   const totalExpectedRecurrings = Math.round(returningUsersRate * vpns.length);
//   const list = exports.setRecurred(returningUsersRate, vpns);
//   const recurList = list.filter(v => v.canRecur);
//   console.assert(list.length === vpns.length, 'wrong length', list.length, vpns.length);
//   console.assert(recurList.length === totalExpectedRecurrings,
//     'wrong recuring length', recurList.length, totalExpectedRecurrings);
//   process.exit();
// };

// exports.testSetRecurred();

exports.findMissingOvpn = (udpDir, tcpDir, regDup) => {
  const udpVPNs = fs.readdirSync(`${__dirname}/../VPN/${udpDir}`)
    .filter(f => /\.ovpn$/.test(f));
  const tcpVPNs = fs.readdirSync(`${__dirname}/../VPN/${tcpDir}`)
    .filter(f => /\.ovpn$/.test(f));
  let found = 0;

  console.log('UPD:', udpVPNs.length, 'TCP:', tcpVPNs.length);
  for (const ovpn of udpVPNs) {
    if (!tcpVPNs.find(f => regDup(ovpn).test(f))) {
      console.log(`${++found}. ${ovpn} not found in TCP folder`);
    }
  }
  found = 0;
  for (const ovpn of tcpVPNs) {
    if (!udpVPNs.find(f => regDup(ovpn).test(f))) {
      console.log(`${++found}. ${ovpn} not found in UDP folder`);
    }
  }
};

exports.extend = (arr, n) => {
  const times = Math.floor(n / arr.length);
  const reminder = n % arr.length;
  let result = [];

  for (let i = 0; i < times; i++) {
    result = result.concat(_.shuffle(arr.slice(0)));
  }
  result = result.concat(_.shuffle(arr.slice(0, reminder)));

  return result;
};

// console.log(exports.extend([1, 5, 9, 2, 4], 11));
// console.log(exports.extend([1, 5, 9, 2, 4], 3));

exports.includedCountries = [
  'AR', 'AT', 'AU', 'BE', 'CA', 'CH', 'CZ', 'DE', 'DK', 'EE', 'FI', 'ES', 'FR', 'GR', 'HK', 'HU',
  'IE', 'IL', 'IS', 'IT', 'JP', 'KR', 'NL', 'NO', 'NZ', 'PL', 'PT', 'SE', 'SG', 'TW', 'UK', 'US', 'ZA'
];

exports._fill = async (vpnName, fnCountries, getRegionFn) => {
  const vpnDir = `${__dirname}/../VPN/${vpnName}`;
  const oVPNs = fs.readdirSync(vpnDir)
    .filter(f => new RegExp(`^(${fnCountries}).*\\.ovpn$`).test(f));

  console.log('saving started');
  for (let i = 0; i < oVPNs.length; i++) {
    const ovpn = oVPNs[i];
    const fullPath = `${vpnDir}/${ovpn}`;
    const ovpnConfigs = fs.readFileSync(fullPath, 'utf8');
    const { serverDNS, region } = getRegionFn(ovpn, ovpnConfigs);
    let vpn = await VPN.findOne({ dns: serverDNS });

    fs.writeFileSync(fullPath, ovpnConfigs.replace(/auth-user-pass( .+)?/, 'auth-user-pass login'));
    if (!vpn) vpn = new VPN({ dns: serverDNS });
    try {
      vpn.ip = await resolve4(serverDNS);
      if (!vpn.ip) throw new Error('unreachable');
    } catch (ex) {
      vpn.lastErrorMsg = `ERROR: ${serverDNS} is unreachable`;
      console.error(vpn.lastErrorMsg);
    }
    vpn.owner = vpnName;
    vpn.config = await this.zipConfig(vpnName, ovpn);
    vpn.region = region;
    vpn.country = countries[this.includedCountries.find(c => new RegExp(`^${c}`, 'i').test(vpn.region))];
    if (!vpn.country) {
      console.error(`ERROR: Country is undefined! Region "${vpn.region}" does not start with valid country code. ${ovpn}`);
    } else {
      await vpn.save();
      // console.log(`${vpn.country} - ${vpn.region}: ${vpn.dns} (${vpn.ip || ''}) saved`);
    }
  }
  console.log(`Generated ${vpnName} VPN configs from ${oVPNs.length} locations`);
};

exports.run = async (vpnName, filteredCountries, getRegionFn) => {
  // mongodump/restore is faster than: process.env.NODE_ENV = 'production';
  await require('../config').dbConnect();
  await this._fill(vpnName, filteredCountries, getRegionFn);
  // fs.writeFileSync('tmp/vpn.zip', (await VPN.findOne({ owner: vpnName })).config);
  process.exit();
};

/**
 * Zip VPN/${vpnOwner} contents and return Buffer
 * @param {String} vpnOwner VPN provider
 * @param {String} ovpnFile One and only *.ovpn file
 */
exports.zipConfig = async (vpnOwner, ovpnFile) => {
  const zip = new Zip();

  if (!vpnOwner || !ovpnFile) {
    console.error('params are missing!');
    return;
  }
  (await glob('**/!(*.ovpn)', { cwd: `VPN/${vpnOwner}` })).forEach((f) => {
    const localFile = `VPN/${vpnOwner}/${f}`;

    if (fs.lstatSync(localFile).isFile()) {
      let dir = path.dirname(f);

      if (dir === '.') {
        dir = '';
      }
      zip.addLocalFile(localFile, dir);
    }
  });
  zip.addLocalFile(`VPN/${vpnOwner}/${ovpnFile}`);

  return zip.toBuffer();
};

// DEPRECATED
// exports.generateVPNsOld = async (boss, sessions) => {
//   const clicks = await this.getVPNs(boss, 'clicks');
//   const views = await this.getVPNs(boss, 'views');
//   const lanes = [...Array(boss.lanes)].map((x, i) => i + 1);
//   const counters = _.fill(Array(boss.lanes), 0);
//   let tmpClicks = _.shuffle(clicks);
//   let tmpViews = _.shuffle(views);
//   let vpn;

//   for (let i = 0; ; i++) {
//     const j = i % lanes.length;
//     const l = lanes[j];
//     const s = _.find(sessions, {
//       lane: l,
//       laneIdx: counters[l - 1]++
//     });

//     if (!s) {
//       lanes.splice(j, 1);
//       if (!lanes.length) break;
//       continue;
//     }
//     if (s.click) {
//       vpn = tmpClicks.pop();
//       s.VPN = vpn;
//       if (vpn.canRecur) s.sessionRecur = vpn.canRecur;
//       if (!tmpClicks.length) tmpClicks = _.shuffle(clicks);
//       continue;
//     }
//     vpn = tmpViews.pop();
//     s.VPN = vpn;
//     if (vpn.canRecur) s.sessionRecur = vpn.canRecur;
//     if (!tmpViews.length) tmpViews = _.shuffle(views);
//   }
// };

exports.marginalize = (list, space) => {
  let temp;
  const run = (res) => {
    for (let i = 0; i < res.length - space; i++) {
      for (let j = i + 1; j < i + space + 1; j++) {
        if (res[i].dns === res[j].dns) {
          for (let f = i + space + 1; f < res.length; f++) {
            if (res[i].dns !== res[f].dns) {
              temp = res[j];
              res[j] = res[f];
              res[f] = temp;
              break;
            }
          }
        }
      }
    }
    return res;
  };
  const r = run(list.slice()).reverse();

  return run(r).reverse();
};

exports.testMarginalize = () => {
  const convert = v => ({ dns: v });
  const arr = [4, 7, 3, 4, 9, 10, 10].map(convert);
  const expected = [4, 7, 10, 9, 4, 3, 10].map(convert);
  const result = exports.marginalize(arr, 3);

  console.assert(_.isEqual(expected, result), 'not equal', result.map(v => v.dns));
};

// exports.testMarginalize();

exports.setVPNtoSessions = (boss, vpns, sessions) => {
  const sortedSessions = sessions.slice().sort((a, b) => a.laneIdx - b.laneIdx || a.lane - b.lane);
  const totalRU = Math.round(sortedSessions.length * boss.returningUsersRate);
  const shuffleVPNs = list => this.marginalize(_.shuffle(list), boss.lanes * 2);
  const inits = shuffleVPNs(this.extend(vpns, totalRU)).map((v, i) => {
    const newV = Object.assign({}, v, {
      session: sortedSessions[i]
    });
    sortedSessions[i].VPN = newV;
    console.assert(newV.config, 'no VPN config', newV);
    return newV;
  });
  const rest = shuffleVPNs(inits.slice().concat(this.extend(vpns, sortedSessions.length - (totalRU * 2))));

  console.assert(sessions.length === inits.length + rest.length,
    'length must be equal', sessions.length, '=', inits.length, '+', rest.length);
  rest.forEach((v, i) => {
    sortedSessions[totalRU + i].VPN = v;
    sortedSessions[totalRU + i].prevSession = v.session;
    console.assert(v.config, 'no VPN config 2', v);
  });

  const filterOutOfOrder = s => s.prevSession && s.prevSession.exitAt >= s.visitAt;

  sortedSessions.filter(filterOutOfOrder).forEach((s) => {
    s.prevSession.prevSession = s;
    s.prevSession = null;
  });
  sortedSessions.filter(filterOutOfOrder).forEach((s) => {
    s.prevSession = null;
  });
};

exports.generateVPNs = async (boss, sessions) => {
  if (_.some(boss.vpnProviders, 'clicksOnly')) {
    throw new Error('clicksOnly is not supported yet');
  }

  for (const provider of boss.vpnProviders) {
    const vpns = await this.getVPNs(boss, provider.owner);
    this.setVPNtoSessions(boss, vpns, provider.allowedLanes
      ? sessions.filter(s => ~provider.allowedLanes.indexOf(s.lane))
      : sessions);
  }
};

exports.testZip = (fileBuffer, expectedFiles = 2) => {
  const fs = require('fs');
  const zipPath = 'tmp/test.zip';

  fs.unlinkSync(zipPath);
  fs.writeFileSync(zipPath, fileBuffer);

  const zipFiles = new Zip(zipPath).getEntries();

  console.assert(zipFiles.length === expectedFiles, 'wrong file length in zips');
  console.assert(zipFiles.find(f => /ovpn$/.test(f.entryName)), 'no *.ovpn file present');
  console.assert(zipFiles.find(f => /login/.test(f.entryName)), 'no login file present');
};

exports.test = async () => {
  const Boss = require('../models/Boss');
  const boss = new Boss({
    lanes: 5,
    sessions: {
      total: 1000
    },
    vpnProviders: [{
      owner: 'HideMyAss',
      allowedLanes: [1, 2, 3]
    }, {
      owner: 'ExpressVPN',
      allowedLanes: [4, 5]
    }],
    returningUsersRate: 0.4
  });
  const { sessions } = require('./sessions').generateSessions(boss);

  await require('../config').dbConnect();
  await exports.generateVPNs(boss, sessions);
  sessions.forEach(s => console.assert(s.VPN, 'no VPN defined'));
  _.range(1, 25, 5).forEach(i => exports.testZip(sessions[i].VPN.config.buffer));

  const rus = sessions.filter(s => s.prevSession);
  const expectedTotalRU = Math.round(boss.returningUsersRate * boss.sessions.total);
  console.assert(rus.length === expectedTotalRU,
    'total prevSessions are wrong', rus.length, expectedTotalRU);

  const dupRU = sessions.filter(s => sessions
    .indexOf(s.prevSession) !== sessions.lastIndexOf(s.prevSession)).length;
  console.assert(!dupRU, 'not every prevSession is unique', dupRU);

  const outOfOrder = rus.filter(s => s.prevSession.exitAt >= s.visitAt);
  console.assert(!outOfOrder.length, 'prevSessions times are not well ordered', outOfOrder);

  const dissalowedVPNs = sessions.filter(s => s.lane === 4 && s.VPN.owner === 'HideMyAss').length
    + sessions.filter(s => s.lane === 2 && s.VPN.owner === 'ExpressVPN').length;
  console.assert(!dissalowedVPNs, 'vpn allowedLanes is not respected', dissalowedVPNs);

  process.exit();
};

// exports.test();
