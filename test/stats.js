const chance = new (require('chance'))();
const request = require('supertest');
const t = require('tap');
const { app } = require('../app');
const Stat = require('../models/Stat');
const statsCtl = require('../controllers/stats');
const IP = require('../models/IP');

const h = {
  badIP: '230.123.321.175',
  goodIPs: [
    chance.ip(),
    chance.ipv6(),
    chance.ip()
  ],
  cSlot: '985686987',
  // cSpell:ignore jjvmyc, qbxbp
  session1: 'ie06jjvmyc0elx3qbxbp',
  session2: 'uq8bjj8vty0ma7h9w9mp',
  UAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/601.7.7 (KHTML, like Gecko) Version/9.1.2 Safari/601.7.7',
  UAgent_bot: 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)'
};
const cleanup = async () => {
  await Stat.deleteMany();
};
const createStats = async (dates) => {
  for (const d of dates) {
    const stat = await statsCtl.create({
      body: {
        clickSlot: h.cSlot,
        post: chance.first(),
        ip: h.goodIPs[2],
        isProxy: false,
        sessionId: h.session1
      },
      get: () => h.UAgent
    }, {
      sendStatus: s => s
    });
    stat.senders[stat.senders.length - 1].date = new Date(d);
    await stat.save();
  }
};
const testStatsLength = (t, expected) => (res) => {
  let i = 0;

  res.body.stats.forEach((s) => {
    i += s.senders.length;
  });
  t.equal(i, expected);
};
const createStatWithOneField = (t) => {
  request(app)
    .post('/api/stats')
    .send({
      ip: h.goodIPs[1],
      door: 'exit',
      dpi: undefined,
      sessionId: h.session2
    })
    .expect(200)
    .catch(t.fail)
    .finally(t.end);
};

t.test('Clean up', cleanup);

t.test('refuse creating when empty request', (t) => {
  request(app)
    .post('/api/stats')
    .expect(422)
    .catch(t.fail)
    .finally(t.end);
});

t.test('refuse creating when given bad IP', (t) => {
  request(app)
    .post('/api/stats')
    .send({ ip: h.badIP })
    .expect(422)
    .catch(t.fail)
    .finally(t.end);
});

t.test('refuse creating when UserAgent is empty', (t) => {
  request(app)
    .post('/api/stats')
    .set('User-Agent', '')
    .send({
      ip: h.goodIPs[2],
      sessionId: h.session1
    })
    .expect(422, {
      'user-agent': {
        location: 'headers',
        param: 'user-agent',
        value: '',
        msg: 'No User-Agent header!'
      }
    })
    .catch(t.fail)
    .finally(t.end);
});

t.test('create statistic', (t) => {
  request(app)
    .post('/api/stats')
    .send({
      ip: h.goodIPs[0],
      isProxy: true,
      post: chance.first(),
      door: 'enter',
      sessionId: h.session1
    })
    .expect(200)
    .catch(t.fail)
    .finally(t.end);
});

t.test('create statistic without post', createStatWithOneField);

t.test('duplicate statistic', createStatWithOneField);

t.test('create click statistic', (t) => {
  request(app)
    .post('/api/stats')
    .set('User-Agent', h.UAgent)
    .send({
      ip: h.goodIPs[2],
      post: chance.first(),
      clickSlot: h.cSlot,
      dpi: 2.1,
      screenSize: '375x667',
      sessionId: h.session2
    })
    .expect(200)
    .catch(t.fail)
    .finally(t.end);
});

t.only('get statistics', async (t) => {
  await IP.deleteMany();
  for (let i = 0; i < h.goodIPs.length - 1; i++) {
    const ip = new IP({
      ip: h.goodIPs[i],
      quality: {
        ipqualityscore: {
          vpn: true,
          fraud_score: 50 + (i * 25)
        }
      }
    });
    ip.quality.markModified('ipqualityscore');
    ip.markModified('quality');
    await ip.save();
  }

  await t.test('subtest', (t) => {
    request(app)
      .get('/api/stats?dirtiness=100')
      .expect(200)
      .then((res) => {
        const { stats } = res.body;
        t.ok(stats.length);

        const stat = stats[0];
        t.equal(stat.ip, h.goodIPs[0]);
        t.ok(stat.isProxy);
        t.ok(stat.senders[0].post.length);
        t.notOk(stat.senders[0].clickSlot);
        t.equal(stat.senders[0].door, 'enter');
        t.notOk(stat.senders[0].dpi);
        t.equal(stat.senders[0].sessionId, h.session1);
        t.equal(stat.IP_quality.length, 1);
        t.equal(stat.IP_quality[0].quality.ipqualityscore.fraud_score, 50);

        t.notOk(stats[1].isProxy);
        t.equal(stats[1].senders.length, 2);
        t.ok(Date.parse(stats[1].senders[1].date) > Date.now() - 20000);
        t.ok(stats[1].IP_quality[0].quality.ipqualityscore.vpn);
        t.equal(stats[1].IP_quality[0].quality.ipqualityscore.fraud_score, 75);
        t.equal(stats[1].senders[0].door, 'exit');
        t.equal(stats[1].senders[0].sessionId, stats[1].senders[1].sessionId);

        const sender = stats[2].senders[0];
        t.equal(sender.UserAgent, h.UAgent);
        t.equal(sender.UserDetected.os.name, 'Mac');
        t.equal(sender.UserDetected.client.name, 'Safari');
        t.equal(sender.clickSlot, h.cSlot);
        t.notOk(sender.door);
        t.equal(sender.dpi, '2.1');
        t.equal(sender.screenSize, '375x667');
        t.equal(sender.sessionId, h.session2);
      })
      .catch(t.fail)
      .finally(t.end);
  });
});

t.test('get clean statistics', (t) => {
  request(app)
    .get('/api/stats')
    .expect(200)
    .then((res) => {
      const { stats } = res.body;

      t.equal(stats.length, 1);
      t.notOk(stats[0].IP_quality.length);
    })
    .catch(t.fail)
    .finally(t.end);
});

t.test('Clean up', cleanup);

t.test('create stats with dates', async () => {
  await createStats(['2015-05-05', '2015-05-01', '2015-05-15']);
  h.goodIPs[2] = chance.ip();
  await createStats(['2015-05-03', '2015-05-11', '2015-05-01', '2015-05-16']);
});

t.test('get stats between dates', (t) => {
  request(app)
    .get(`/api/stats?from=${new Date('2015-05-02').getTime()}&to=${new Date('2015-05-15').getTime()}`)
    .expect(200)
    .then(testStatsLength(t, 4))
    .catch(t.fail)
    .finally(t.end);
});

t.test('get stats after date', (t) => {
  request(app)
    .get(`/api/stats?from=${new Date('2015-05-14').getTime()}`)
    .expect(200)
    .then(testStatsLength(t, 2))
    .catch(t.fail)
    .finally(t.end);
});

t.test('get stats before date', (t) => {
  request(app)
    .get(`/api/stats?to=${new Date('2015-05-02').getTime()}`)
    .expect(200)
    .then(testStatsLength(t, 2))
    .catch(t.fail)
    .finally(t.end);
});

t.test('Clean up', cleanup);

t.test('allow creating statistic from bot or crawler', (t) => {
  request(app)
    .post('/api/stats')
    .set('User-Agent', h.UAgent_bot)
    .send({
      ip: h.goodIPs[0],
      sessionId: h.session2
    })
    .expect(200)
    .catch(t.fail)
    .finally(t.end);
});

t.test('refuse getting bot stats', (t) => {
  request(app)
    .get('/api/stats')
    .expect(200)
    .then((res) => {
      t.notOk(res.body.stats.length);
    })
    .catch(t.fail)
    .finally(t.end);
});

t.tearDown(process.exit);
