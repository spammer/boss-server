const crawlersMatch = {
  $match: {
    'senders.UserAgent': {
      $not: /bot|crawler|facebook|wordpress|google/i
    }
  }
};
/* global db, print, tojson */
/* eslint no-restricted-globals: ["error", "event"] */
const c = db.getCollection('stats').aggregate([{
  $unwind: '$senders'
}, crawlersMatch, {
  $match: {
    'senders.UserDetected.device.model': 'iPhone 6s'
  }
}, {
  $project: {
    _id: 0,
    i: 1,
    UserAgent: '$senders.UserAgent'
    // ,UserDetected: '$senders.UserDetected'
  }
}]);

let i = 0;

print('[');
while (c.hasNext()) {
  const o = c.next();

  o.i = ++i;
  print(`${tojson(o)},`);
}
print('null]');
