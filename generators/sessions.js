const _ = require('lodash');
const Boss = require('../models/Boss');

exports.randomizeArrayItems = (array, max) => {
  let origSum = 0, newSum = 0;

  array = array.map((item) => {
    origSum += item;
    item = _.random(item - max, item + max);
    newSum += item;

    return item;
  });

  const avg = origSum / array.length;
  let rest = newSum - origSum;

  if (rest > 0) {
    while (rest > 0) {
      if (rest - max < 0) {
        max = rest;
        rest = 0;
      } else rest -= max;
      array[array.findIndex(v => v > avg)] -= max;
    }
  } else {
    while (rest < 0) {
      if (rest + max > 0) {
        max = -rest;
        rest = 0;
      } else rest += max;
      array[array.findIndex(v => v < avg)] += max;
    }
  }

  return array;
};

// for (let i = 0; i < 20; i++) {
//   const arr = Array.from(new Array(6), () => 6);
//   const newArr = exports.randomizeArrayItems(arr, 5);
//   console.assert(newArr.reduce((acc, b) => acc + b, 0) === 36, 'sum disbalance');
//   console.assert(newArr.find(v => v > 0 && v < 12), 'limit breach');
// }

class Exponentializer {
  constructor(xLength, total, min) {
    Object.assign(this, {
      sortFn: (a, b) => a - b,
      total,
      xLength,
      xData: Array.from(new Array(+xLength), (d, i) => i + 1),
      sumXRatio: 0,
      sumX: 0,
      xStops: [],
      yData: [],
      min,
      sum: 0
    });
  }

  static getTangent(opposite, adjacent) {
    return opposite / ((adjacent / 2) * (adjacent + 1));
  }

  createCurve(xRatio, toalRatio, noiseRatio = 0.5) {
    const x = Math.round(this.xLength * xRatio);

    this.sumX += x;
    if (this.sumX >= this.xLength) {
      this.sumX = this.xLength;
      this.sumXRatio = 1;
      return;
    }
    this.xStops.push(this.sumX);
    this.sumXRatio += xRatio;
    if (this.sumXRatio > 1) throw new Error('Sum of x ratios is over 1');
    this.yData.sort(this.sortFn);

    const lastMaxH = _.last(this.yData) || 0;
    const tan = Exponentializer.getTangent(this.total * toalRatio, x);

    this.yData = this.yData.concat(this.xData.slice(0, x).map((i) => {
      let h = i * tan;

      h += lastMaxH + ((Math.random() < 0.5 ? -1 : 1) * _.random(0, h * noiseRatio));
      if (h < this.min) h = _.random(this.min, this.min + 0.49);
      h = Math.round(h);
      this.sum += h;

      return h;
    }));
  }

  createCurves(data) {
    data.forEach(d => this.createCurve(...d));
    this.createEndLine();
    return this;
  }

  createEndLine() {
    let x = this.xLength - this.sumX;

    if (!x) x = 1;
    this.yData.sort(this.sortFn);

    const lastMaxH = _.last(this.yData) || 0;
    const opposite = this.total - this.sum - (lastMaxH * x);
    if (opposite < 0) console.error(opposite, lastMaxH, this.sum, this.total);
    const tan = Exponentializer.getTangent(opposite, x);
    let h, diff = NaN;

    this.yData = this.yData.concat(this.xData.slice(0, x).map((i) => {
      h = Math.round(lastMaxH + (i * tan));
      this.sum += h;
      return h;
    }));
    diff = this.total - Math.round(this.sum);
    // TODO: observe diff on chart, happens rarely
    // if (diff) throw new Error(`total =/= sums, diff: ${diff}`);
    this.yData[this.yData.length - 1] += diff;
  }
}

exports.generateSessions = (c) => {
  const totalBouncedSessions = Math.round(c.sessions.total * c.sessions.bounceRate);
  const totalInteractiveSessions = c.sessions.total - totalBouncedSessions;
  const totalInteractiveSessionsTime = c.sessions.avgSessionTime * c.sessions.total;
  const interactiveSessions = new Exponentializer(totalInteractiveSessions,
    totalInteractiveSessionsTime, c.sessions.minTime.interactive).createCurves([
    [0.5, 0.11, 0.8],
    [0.22, 0.02, 0.6],
    [0.1, 0.01, 0.3],
    [0.08, 0.01, 0.1],
    [0.025, 0.005, 0],
    [0.02, 0.04, 0]
  ]).yData.map(v => ({ duration: v }));
  let totalBouncedSessionsTime = 0;
  const bouncedSessions = Array.from(new Array(totalBouncedSessions), () => {
    const t = _.random(c.sessions.minTime.bouncedSession, c.sessions.minTime.bouncedSession * 2);
    totalBouncedSessionsTime += t;
    return {
      duration: t,
      bounced: true
    };
  });
  const totalSessionsTime = totalBouncedSessionsTime + totalInteractiveSessionsTime;
  let totalMinSpaceTime = 0;
  const minSpace = Array.from(new Array(+c.sessions.total), () => {
    const t = _.random(c.sessions.minTime.betweenSessions, c.sessions.minTime.betweenSessions * 2);
    totalMinSpaceTime += t;
    return t;
  });
  const totalMinTime = totalSessionsTime + totalMinSpaceTime;
  const lanes = Math.ceil(totalMinTime / c.duration);

  if (lanes > c.lanes) throw new Error(`Can't fit, ${lanes} lanes required.`);

  const totalSpace = (c.lanes * c.duration) - totalSessionsTime;
  const avgSpareSpace = Math.round((totalSpace - totalMinSpaceTime) / c.sessions.total);
  const spaces = this.randomizeArrayItems(Array.from(new Array(+c.sessions.total),
    (d, i) => minSpace[i] + avgSpareSpace), avgSpareSpace);
  const clicks = [], until = Date.parse(c.since) + c.duration, maxExits = [];
  let spaceIdx = 0, start = Date.parse(c.since), lane = 1, laneIdx = 0, shortest;
  const sessions = _.shuffle(interactiveSessions.concat(bouncedSessions)).map((s, i) => {
    s.duration = +s.duration.toFixed(2);
    if (start + s.duration > until) {
      maxExits.push({ l: lane++, i: laneIdx, s: start });
      laneIdx = 0;
      start = Date.parse(c.since);
    }
    start += spaces[spaceIdx++];
    s.visitAt = start;
    s.exitAt = start + s.duration;
    s.lane = lane;
    s.laneIdx = laneIdx++;
    if (s.duration > c.sessions.minTime.beforeAdClick) clicks.push(i);
    start = s.exitAt;
    return s;
  });
  spaceIdx = 0;
  if (lane > c.lanes) {
    sessions.filter(s => s.lane > c.lanes).forEach((s) => {
      shortest = _.minBy(maxExits, e => e.s);
      s.lane = shortest.l;
      s.laneIdx = shortest.i++;
      s.visitAt = shortest.s + minSpace[spaceIdx++];
      s.exitAt = s.visitAt + s.duration;
      if (s.exitAt > until) {
        console.error(`session ${s.lane}:${s.laneIdx} is over the edge`);
      }
      shortest.s = s.exitAt;
    });
  }

  _.shuffle(clicks).slice(0, Math.ceil(c.ctr * c.sessions.total)).forEach((i) => {
    sessions[i].adClick = true;
  });

  return {
    sessions
  };
};

exports.generateLinks = (c, sessions) => {
  const interactiveSessions = sessions.filter(s => !s.bounced);
  let extraPages = Math.round(c.sessions.total * (c.sessions.pagesPerSession - 1));
  const createLinks = (session, maxPages) => {
    let pages = _.random(1, maxPages - 1);

    if (extraPages - pages < 0) {
      pages = extraPages;
      extraPages = 0;
    } else extraPages -= pages;
    for (let i = 0; i < pages; i++) {
      session.links.unshift({ selectors: c.sessions.pageSelectors.exceptLast });
    }
  };
  const lastPS = _.last(c.sessions.pageSpawn);
  const findPS = s => c.sessions.pageSpawn.find(ps => s.duration <= ps.maxTime) || lastPS;
  let sIdx = 0, session, pages;

  if (extraPages < interactiveSessions.length) throw new Error('Not enough pages to fit in');
  interactiveSessions.forEach((s) => {
    s.links = [{
      selectors: c.sessions.pageSelectors.last,
      time: s.duration - c.sessions.minTime.beforeLastClick
    }];
    extraPages--;
  });
  while (extraPages) {
    session = interactiveSessions[sIdx++ % interactiveSessions.length];
    pages = session.duration > lastPS.maxTime
      ? lastPS.maxPages + (lastPS.maxPages * Math.ceil(session.duration / lastPS.maxTime))
      : pages = findPS(session).maxPages;
    if (pages > 1) createLinks(session, pages);
  }
  interactiveSessions.forEach((s) => {
    const lastTime = _.last(s.links).time;
    const size = s.links.length;
    let min = c.sessions.minTime.betweenClicks, t;

    if (size < 2) return;
    for (let i = size - 1; i > 0; i--) {
      t = _.random(min, lastTime - (i * c.sessions.minTime.betweenClicks));
      s.links[size - i - 1].time = t;
      min = t + c.sessions.minTime.betweenClicks;
    }
  });
};

exports.testOnDB = async () => {
  await require('../config').dbConnect();

  const boss = new Boss({
    lanes: 5,
    sessions: {
      total: '1234',
      bounceRate: 0.85
    }
  });
  const Command = require('../models/Command');
  const vpn = await require('../models/VPN').findOne({ owner: 'StrongVPN' });
  const { sessions } = exports.generateSessions(boss);

  exports.generateLinks(boss, sessions);
  await Command.deleteMany({ UserAgent: 'empty' });
  await Command.insertMany(sessions.map(s => ({
    VPN: vpn,
    UserAgent: 'empty',
    emulator: 'empty',
    posts: ['http://localhost:8081/emptyLink'],
    visitAt: s.visitAt,
    exitAt: s.exitAt,
    lane: s.lane,
    links: s.links
  })));
  console.assert((await Command.find({ UserAgent: 'empty' }))
    .length === +boss.sessions.total, 'total sessions not ok');
  console.assert((await Command.find({ UserAgent: 'empty', 'links.selectors': /prev/ }))
    .length > 0, 'selectors not ok');
  console.log(await Command.aggregate([{
    $match: { UserAgent: 'empty' }
  }, {
    $project: {
      count: { $size: '$links' },
      exitAt: 1,
      visitAt: 1,
      duration: { $subtract: ['$exitAt', '$visitAt'] }
    }
  }, {
    $group: {
      _id: '$count',
      f: { $sum: 1 },
      d: { $min: '$duration' }
    }
  }, {
    $project: {
      _id: 0,
      linkSize: '$_id',
      frequency: '$f',
      minDuration: '$d'
    }
  }, {
    $sort: { linkSize: 1 }
  }]));
};

exports.test = () => {
  let sumTime = 0, sumBounced = 0, sumPages = 0, emptyLinks = 0, badDates = 0;
  let ceilBroken = 0, spaceDisrespected = 0, selectorsBroken = 0;
  const boss = new Boss({
    lanes: 5,
    sessions: {
      total: '1234',
      bounceRate: 0.85
    }
  });
  const totalExtraPages = Math.round(boss.sessions.total * (boss.sessions.pagesPerSession - 1));
  const checkSessionFn = (s) => {
    if (s.links) {
      const last = _.last(s.links);
      const t = last.time;

      if (!s.links.length) emptyLinks++;
      sumPages += s.links.length;
      sumTime += t;
      for (let i = 0; i < s.links.length - 1; i++) {
        if (s.links[i + 1].time - s.links[i].time < boss.sessions.minTime.betweenClicks) {
          console.error(s);
          spaceDisrespected++;
        }
      }
      if (s.duration <= boss.sessions.pageSpawn[1].maxTime && s.links.length > 1) ceilBroken++;
      if (t > s.duration) ceilBroken++;
      if (s.links.filter(l => !l.selectors).length > 0) selectorsBroken++;
      if (boss.sessions.pageSelectors.exceptLast === last.selectors) selectorsBroken++;
    } else sumBounced++;
    if (!s.visitAt || !s.exitAt) badDates++;
  };
  const { sessions } = exports.generateSessions(boss);

  exports.generateLinks(boss, sessions);
  console.assert(!emptyLinks, 'links != empty arrays', emptyLinks);
  sessions.forEach(checkSessionFn);
  for (let i = 1; i <= boss.lanes; i++) {
    const ls = sessions.filter(s => s.lane === i);
    const maxIdx = _.maxBy(ls, 'laneIdx').laneIdx;
    console.assert(maxIdx + 1 === ls.length,
      'lane max idx !== lane length', maxIdx + 1, ls.length, i);
    const sumMax = (maxIdx / 2) * (maxIdx + 1);
    const sum = ls.reduce((a, b) => a + b.laneIdx, 0);
    console.assert(sum === sumMax, 'lane sum max idx !== lane sum', sum, sumMax, i);
  }
  console.assert(sessions.length === +boss.sessions.total, 'total sessions are wrong');
  console.assert(totalExtraPages === sumPages, 'sum of total pages is wrong');
  console.assert(Math.round(sumTime / boss.sessions.total) === +boss.sessions.avgSessionTime,
    'avgSessionTime is not ok');
  console.assert(!spaceDisrespected,
    'space between clicks is not respected', spaceDisrespected);
  console.assert(!ceilBroken, 'limits are not respected', ceilBroken);
  console.assert(!selectorsBroken, 'selectors are not well distributed', selectorsBroken);
  console.assert(Math.round(boss.sessions.bounceRate * boss.sessions.total) === sumBounced,
    'bounceRate is not ok', sumBounced);
  console.assert(!badDates, 'date fields are not defined', badDates);
};

// exports.test();
// exports.testOnDB().then(process.exit);
