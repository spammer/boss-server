const _ = require('lodash');
const { dbConnect } = require('../config');
const countires = require('../data/countries');
const VPN = require('../models/VPN');

const analyseVPNs = async () => {
  await dbConnect();
  VPN.aggregate([{
    $group: {
      _id: '$owner',
      count: { $sum: 1 }
    }
  }]);
  const getMask = ip => ip.substr(0, ip.lastIndexOf('.'));
  const b24 = {}, b16 = {};
  let b, sum = 0, unique = 0;
  (await VPN.find({
    owner: 'StrongVPN',
    lastErrorMsg: null
    // country: 'United States'
  })).forEach((v) => {
    if (!v.ip) return;
    b = getMask(v.ip);
    b24[b] = (b24[b] + 1) || 1;
    b = getMask(b);
    b16[b] = (b16[b] + 1) || 1;
  });
  Object.entries(b24).sort((a, b) => b[1] - a[1]).forEach(([k, v]) => {
    if (v === 1) unique++;
    else {
      console.log(`${k}: ${v}`);
      sum++;
    }
  });
  console.log(`total /24 groups: ${sum}, non-grouped IPs: ${unique}`);
  process.exit();
};

const analyseNordVPN = async () => {
  await dbConnect();
  const dbResults = await VPN.aggregate([{
    $match: {
      owner: 'NordVPN'
    }
  }]);
  const map = {};
  const sorted = [];

  dbResults.forEach((vpn) => {
    map[vpn.country] = map[vpn.country] + 1 || 1;
  });
  _.forEach(map, (v, k) => {
    sorted.push({
      country: countires[k],
      code: k,
      occurrences: v,
      freq: `${Math.round((v / dbResults.length) * 100)}%`,
    });
  });
  sorted
    .sort((a, b) => a.occurrences - b.occurrences)
    .forEach(s => console.log(`${s.country} (${s.code}): ${s.occurrences} (${s.freq})`));
  process.exit();
};

analyseVPNs().then(analyseNordVPN);
