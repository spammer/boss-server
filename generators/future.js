// npm run future --del=all --total=1234
const { dbConnect } = require('../config');
const { generatePosts } = require('../generators/posts');
const { generateSessions, generateLinks } = require('../generators/sessions');
const { generateUAs } = require('../generators/user-agents');
const { generateVPNs } = require('../generators/vpn-configs');
const Boss = require('../models/Boss');
const Command = require('../models/Command');

exports.generateFuture = async () => {
  const cmdDeleteOpts = {};

  await dbConnect();
  if (process.env.npm_config_del === 'all' || process.env.NODE_ENV !== 'production') {
    await Boss.deleteMany();
  } else {
    cmdDeleteOpts.executed = false;
  }
  await Command.deleteMany(cmdDeleteOpts);

  const boss = new Boss();
  const { sessions } = generateSessions(boss);
  const posts = await generatePosts(boss);
  const uas = await generateUAs(boss.sessions.total, boss.ctr);

  console.log(`Generated ${boss.lanes} lanes. Now saving...`);
  generateLinks(boss, sessions);
  await generateVPNs(boss, sessions);
  await boss.save();
  for (let i = 0; i < boss.sessions.total; i++) {
    const s = sessions[i];
    const cmd = new Command();

    cmd.boss = boss;
    cmd.VPN = s.VPN;
    cmd.UserAgent = uas[i].UAString;
    cmd.emulator = uas[i].emulator;
    cmd.referer = 'https://m.facebook.com/';
    cmd.visitAt = s.visitAt;
    cmd.exitAt = s.exitAt;
    cmd.lane = s.lane;
    // cmd.duration = cmd.exitAt - cmd.visitAt; // TODO: check bot usage before removed
    if (s.click) {
      cmd.posts = posts.nextClick(s.duration);
      cmd.ad = {
        postIdx: cmd.posts.length - 1
      };
    } else {
      cmd.posts = posts.nextView(s.duration);
    }
    cmd.links = s.links;
    cmd.test = boss.test;
    await cmd.save();
    s._id = cmd._id;
  }
  await Promise.all(sessions
    .filter(s => s.prevSession)
    .map(s => Command.updateOne({ _id: s._id }, { $set: { prevSession: s.prevSession._id } })));
  console.log('finished:', await Command.countDocuments({
    executed: false
  }), 'sessions');
};

exports.run = async () => {
  try {
    await this.generateFuture();
  } catch (ex) { console.error(ex); }
  process.exit();
};

exports.run();
