const { includedCountries, run } = require('../vpn-configs');

// require('../vpn-configs').findMissingOvpn('NordVPN/ovpn_udp', 'NordVPN/ovpn_tcp',
//   ovpn => new RegExp(`^${ovpn.match(/([^.]+)\..+ovpn$/)[1]}\\.`));

const filteredCountries = includedCountries.join('|').toLowerCase();
const getRegionFn = ovpn => ({
  serverDNS: /(.+\.nordvpn\.com)/.exec(ovpn)[1],
  region: /([^\d]+).+\.nordvpn\.com/.exec(ovpn)[1]
});

run('NordVPN', filteredCountries, getRegionFn);
