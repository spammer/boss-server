const mongoose = require('mongoose');

module.exports = mongoose.model('FakeEmail', new mongoose.Schema({
  first: String,
  last: String,
  gender: String,
  username: String,
  password: {
    type: String,
    required: true
  },
  webmail: String,
  refEmail: String,
  IP: String,
  phone: String,
  birthday: Date
}, { timestamps: true }));

exports.generate = async (amount) => {
  const faker = require('faker');
  const FakeEmail = module.exports;
  const email = {
    gender: '',
    webmail: '',
    refEmail: '',
    IP: ''
  };
  const xmlSafeChars = /[^ &<>\\]/;
  let output = [];

  await require('../config').dbConnect();
  await FakeEmail.deleteMany();
  for (let i = 0; i < amount; i++) {
    // check en locale (male_first_name and female_first_name) for genders
    email.first = faker.name.firstName();
    email.last = faker.name.lastName();
    email.password = faker.internet.password(12, false, xmlSafeChars);
    // email.phone = faker.phone.phoneNumberFormat(2);
    email.username = faker.internet.userName(email.first, email.last)
      .replace(/[^a-zA-Z-0-9]/g, '') + faker.random.number(10, 999);
    email.birthday = faker.date.past(50, new Date('Sat Sep 20 1992 21:35:02'));
    await new FakeEmail(email).save();
    output += `\nuser: ${email.username}\npass: ${email.password}\nnames: ${email.first} ${email.last}\n`;
  }
  require('fs').writeFileSync('data/fake-emails.txt', output);
  // require('child_process').execSync(`mongoexport -d gd -c fakeemails
  //   -o ${__dirname}/../data/fake-emails.json --pretty --jsonArray`);
};

(async () => {
  await this.generate(10);
  process.exit();
})();
