/**
 * Module dependencies.
 */
const express = require('express');
const compression = require('compression');
const session = require('express-session');
const bodyParser = require('body-parser');
const logger = require('morgan');
const chalk = require('chalk');
const errorHandler = require('errorhandler');
const lusca = require('lusca');
const cors = require('cors');
const dotenv = require('dotenv');
const MongoStore = require('connect-mongo')(session);
const flash = require('express-flash');
const path = require('path');
const mongoose = require('mongoose');
const passport = require('passport');
const expressStatusMonitor = require('express-status-monitor');
const sass = require('node-sass-middleware');
const multer = require('multer');
const config = require('./config');
// const opn = require('opn');

const upload = multer({ dest: path.join(__dirname, 'uploads') });

/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 */
dotenv.load({ path: '.env.example' });

/**
 * Controllers (route handlers).
 */
const homeCtl = require('./controllers/home');
const userCtl = require('./controllers/user');
const apiCtl = require('./controllers/api');
const contactCtl = require('./controllers/contact');

const statsCtl = require('./controllers/stats');
const bossCtl = require('./controllers/boss');
const adVariantsCtl = require('./controllers/ad-variants');
const commanderCtl = require('./controllers/commander');

/**
 * API keys and Passport configuration.
 */
const passportConfig = require('./config/passport');

/**
 * Create Express server.
 */
const app = express();
const csrf = lusca.csrf();
const mode = app.get('env');
const staticFolder = p => express.static(path.join(__dirname, p), { maxAge: '1y' });
let logType = '';

app.use(staticFolder('public'));
app.use('/webfonts', staticFolder('node_modules/@fortawesome/fontawesome-free/webfonts'));

if (mode === 'development') {
  app.use(errorHandler());
  logType = 'dev';
} else if (mode === 'production') {
  logType = 'combined';
}
if (logType) {
  app.use(logger(logType));
}

/**
 * Start Express server.
 */
function listen() {
  if (mode === 'test') return;
  const port = app.get('port');
  app.listen(port, () => {
    console.log('%s App is running at http://localhost:%d in %s mode', chalk.green('✓'), port, mode);
    console.log('  Press CTRL-C to stop\n');
    // opn(`http://localhost:${port}/chart`);
  });
}
config.dbConnect();
mongoose.connection
  .on('error', (err) => {
    console.error(err);
    console.log('%s MongoDB connection error. Please make sure MongoDB is running.', chalk.red('✗'));
    process.exit();
  })
  .on('disconnected', config.dbConnect)
  .on('open', listen);

// mongoose.set('debug', function(coll, method, query, doc) {
//   console.log(
//     ' => Query executed: ',
//     '\ncollection => ' + coll,
//     '\nmethod => ' + method,
//     '\ndata => ' + JSON.stringify(query),
//     doc && ('doc => ' + JSON.stringify(doc)), '\n')
// });

/**
 * Express configuration.
 */
app.set('host', process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0');
app.set('port', config.port || process.env.OPENSHIFT_NODEJS_PORT || 8080);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(expressStatusMonitor());
app.use(compression());
app.use(sass({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public')
}));
app.use(bodyParser.json({ limit: '2mb' }));
app.use(bodyParser.urlencoded({ limit: '2mb', extended: true }));
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: process.env.SESSION_SECRET,
  cookie: { maxAge: 1209600000 }, // two weeks in milliseconds
  store: new MongoStore({
    autoReconnect: true,
    url: config.dbUrl
  })
}));
app.use(flash());
if (mode !== 'test') {
  app.use(passport.initialize());
  app.use(passport.session());
  app.use((req, res, next) => {
    if (/^\/api\/(upload|stats|boss)\b/.test(req.path)) {
      next();
    } else {
      csrf(req, res, next);
    }
  });
}
// app.use(lusca.xframe('SAMEORIGIN'));
app.use(lusca.xssProtection(true));
app.disable('x-powered-by');
app.use((req, res, next) => {
  res.locals.user = req.user;
  next();
});
app.use((req, res, next) => {
  // After successful login, redirect back to the intended page
  if (!req.user
    && req.path !== '/login'
    && req.path !== '/signup'
    && !req.path.match(/^\/auth/)
    && !req.path.match(/\./)) {
    req.session.returnTo = req.originalUrl;
  } else if (req.user
    && (req.path === '/account' || req.path.match(/^\/api/))) {
    req.session.returnTo = req.originalUrl;
  }
  next();
});

/**
 * Primary app routes.
 */
app.get('/', homeCtl.index);
app.get('/privacy-policy', (req, res) => { res.render('privacy-policy'); });
app.get('/terms-of-service', (req, res) => { res.render('terms-of-service'); });
app.get('/login', userCtl.getLogin);
app.post('/login', userCtl.postLogin);
app.get('/logout', userCtl.logout);
app.get('/forgot', userCtl.getForgot);
app.post('/forgot', userCtl.postForgot);
app.get('/reset/:token', userCtl.getReset);
app.post('/reset/:token', userCtl.postReset);
app.get('/signup', userCtl.getSignup);
app.post('/signup', userCtl.postSignup);
app.get('/contact', contactCtl.getContact);
app.post('/contact', contactCtl.postContact);
app.get('/account', passportConfig.isAuthenticated, userCtl.getAccount);
app.post('/account/profile', passportConfig.isAuthenticated, userCtl.postUpdateProfile);
app.post('/account/password', passportConfig.isAuthenticated, userCtl.postUpdatePassword);
app.post('/account/delete', passportConfig.isAuthenticated, userCtl.postDeleteAccount);
app.get('/account/unlink/:provider', passportConfig.isAuthenticated, userCtl.getOauthUnlink);

/**
 * API examples routes.
 */
app.get('/api', apiCtl.getApi);
app.use('/api/boss', cors(bossCtl.restrictAccess));
app.get('/api/boss/ad-variants', adVariantsCtl.index);
app.get('/api/boss/ad-variant-html', adVariantsCtl.getHtml);
app.post('/api/boss/ad-variant/:id*?', adVariantsCtl.validatePOST, statsCtl.displayErrors, adVariantsCtl.create);
app.delete('/api/boss/ad-variants', adVariantsCtl.remove);
app.get('/api/boss/commander', commanderCtl.getNextCommand);
app.get('/api/boss/history', commanderCtl.history);
app.post('/api/boss/history', commanderCtl.save);
app.get('/api/boss/report-vpn', commanderCtl.reportVPN);
app.get('/api/boss/session', commanderCtl.validateSessionGET, statsCtl.displayErrors, commanderCtl.getSession);
app.get('/api/stats', statsCtl.isME, statsCtl.index);
app.post('/api/stats', cors({ origin: 'http://beautifull.life', optionsSuccessStatus: 200 }), statsCtl.validatePOST, statsCtl.displayErrors, statsCtl.create);
app.get('/api/stats/ips', statsCtl.isME, statsCtl.ips);
app.get('/api/stats-test', statsCtl.isME, statsCtl.testIndex);
app.get('/api/upload', apiCtl.getFileUpload);
app.post('/api/upload', upload.single('myFile'), apiCtl.postFileUpload);
app.get('/chart', statsCtl.isME, (req, res) => { res.render('chart', { title: 'Chart' }); });
app.get('/test-c3', (req, res) => { res.render('test-c3'); });
app.get('/test-clicks', (req, res) => { res.render('test-clicks'); });
app.get('/test-clicks2', (req, res) => { res.render('test-clicks2'); });
app.get('/test-clicks3', (req, res) => { res.render('test-clicks3'); });
app.get('/test-iframe/:j', (req, res) => { res.render('iframe', { j: req.params.j }); });
app.get('/test-iframe-last', (req, res) => { res.render('iframe-last'); });
app.get('/test-scroll', (req, res) => { res.render('test-scroll'); });
app.get('/test-touch', (req, res) => { res.render('test-touch'); });

module.exports = {
  app,
  connection: mongoose.connection
};
