const request = require('supertest');
const t = require('tap');
const { app } = require('../app');

t.test('GET / - 200 OK', (t) => {
  request(app)
    .get('/')
    .expect(200)
    .catch(t.fail)
    .finally(t.end);
});

t.test('GET /login - 200 OK', (t) => {
  request(app)
    .get('/login')
    .expect(200)
    .catch(t.fail)
    .finally(t.end);
});

t.test('GET /signup - 200 OK', (t) => {
  request(app)
    .get('/signup')
    .expect(200)
    .catch(t.fail)
    .finally(t.end);
});

t.test('GET /api - 200 OK', (t) => {
  request(app)
    .get('/api')
    .expect(200)
    .catch(t.fail)
    .finally(t.end);
});

t.test('GET /contact - 200 OK', (t) => {
  request(app)
    .get('/contact')
    .expect(200)
    .catch(t.fail)
    .finally(t.end);
});

t.test('GET /random-url - 404', (t) => {
  request(app)
    .get('/reset')
    .expect(404)
    .catch(t.fail)
    .finally(t.end);
});

t.tearDown(process.exit);
