const _ = require('lodash');
const cheerio = require('cheerio');
const cliProgress = require('cli-progress');
const fs = require('fs');
const IP = require('../models/IP');
const mongoose = require('mongoose');
const request = require('request');
const url = require('url');
const util = require('util');

process.env.NODE_ENV = 'production';
const config = require('../config');

const arequest = util.promisify(request);
const pbar = new cliProgress.Bar({
  format: 'progress [{bar}] {percentage}% | {value}/{total} | {server} ({serverIP})'
}, cliProgress.Presets.shades_classic);
const resolve4 = util.promisify(require('dns').resolve4);

const listLink = url.parse('https://www.cyberghostvpn.com/en_US/vpn-server');
const listFile = `tmp/${listLink.hostname}.html`;

request(listLink.href, async (err, res, html) => {
  if (err) {
    if (!fs.existsSync(listFile)) {
      console.error(`Can't access '${listLink.href}' nor '${listFile}'`);
      return;
    }
    html = fs.readFileSync(listFile, 'utf8');
    return;
  }
  fs.writeFileSync(listFile, html); // make sure ./tmp exists
  const $ = cheerio.load(html);
  const $servers = $('#collapseCountryUS .server-name');

  pbar.start($servers.length, 0, {
    server: '',
    serverIP: 'checking IP score...'
  });
  await mongoose.connect(config.db, {
    useCreateIndex: true,
    useNewUrlParser: true
  });
  for (let i = 0; i < $servers.length; i++) {
    const server = $($servers[i]).attr('title');
    const serverDNS = `${server}.cg-dialup.net`;
    let serverIP;

    try {
      [serverIP] = await resolve4(serverDNS);
    } catch (ex) {
      console.log(`${serverDNS} is fake DNS`);
      continue;
    }

    let ip = await IP.findOne({ ip: serverIP });

    if (ip) {
      console.log(`${server} (${serverIP}) already exists in DB.`);
      continue;
    }
    ip = new IP({
      ip: serverIP
    });

    const { body } = await arequest(`https://www.ipqualityscore.com/api/json/ip/oy9Erur3IE8o9c3jla6GwXd3Y43dPv5z/${serverIP}?strictness=5`);
    let o;
    try {
      o = JSON.parse(body);
    } catch (ex) {
      console.log(`can't get score for ${serverIP}`);
      console.log(body);
      continue;
    }

    pbar.update(i + 1, { server, serverIP });
    _.assign(ip.quality.ipqualityscore, _.pick(o, _.keys(ip.quality.ipqualityscore)));
    _.assign(ip.location, _.pick(o, _.keys(ip.location)));
    ip.vpn.owner = 'CyberGhostVPN';
    ip.vpn.dns = serverDNS;
    await ip.save();
  }
  pbar.stop();
  process.exit();
}).on('response', (res) => {
  pbar.start(0, 0);
  res.on('data', (chunk) => {
    pbar.update(chunk.length, {
      server: '',
      serverIP: 'downloading server list'
    });
  });
  res.on('end', () => {
    pbar.stop();
  });
});
