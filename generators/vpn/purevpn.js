const countries = require('../../data/countries');
const { includedCountries, fill } = require('../vpn-configs');

// findMissingOvpn('PureVPN/OpenVPN_Config_Files/UDP',
//   'PureVPN/OpenVPN_Config_Files/TCP',
//   ovpn => new RegExp(`${require('lodash').escapeRegExp(ovpn
//     .match(/(.+)-(UD|TC)P\.ovpn/)[1])}-(UD|TC)P\\.ovpn`));

const filteredCountries = Object.entries(countries)
  .filter(([code]) => ~includedCountries.indexOf(code))
  .map(([, country]) => country.replace(' ', '')).join('|')
  .replace('SouthKorea', 'Korea,South');
const getRegionFn = (ovpn, config) => ({
  serverDNS: /remote ([^\s]+)/gm.exec(config)[1],
  region: /^([^-]+).+-(UD|TC)P.ovpn$/.exec(ovpn)[1]
});

exports.run = async () => {
  // mongodump restore is faster than this
  // process.env.NODE_ENV = 'production';
  await require('../../config').dbConnect();
  // TODO: need login file
  await fill('PureVPN', filteredCountries, getRegionFn);
  // fs.writeFileSync('tmp/vpn.zip', (await VPN.findOne({ owner: vpnName })).config);
  process.exit();
};

exports.run();
