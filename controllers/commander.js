const { body, query } = require('express-validator/check');
const moment = require('moment');
const AdPattern = require('../models/AdPattern');
const Boss = require('../models/Boss');
const Command = require('../models/Command');
const VPN = require('../models/VPN');

exports.testCommand = async (lane) => {
  const vpn = await VPN.findOne({ owner: 'ExpressVPN', dns: /newzealand/ });

  // vpn.config = await require('../generators/vpn-configs').zipConfig('StrongVPN', 'master.ovpn');
  /* cSpell:disable */
  return {
    boss: await Boss.findOne(),
    // possibly slow dns: kul102, mia101, mia201, mia306, iad201, iad303, tlv101, ord101, icn201, lax102, lax304, las302, sea102, jfk102, ams104, yul301
    VPN: vpn,
    UserAgent: 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_1_2 like Mac OS X) AppleWebKit/604.3.5 (KHTML, like Gecko) Mobile/15B202 [FBAN/FBIOS;FBAV/175.0.0.47.102;FBBV/112197024;FBDV/iPhone8,4;FBMD/iPhone;FBSN/iOS;FBSV/11.1.2;FBSS/2;FBCR/OPTUS;FBID/phone;FBLC/en_GB;FBOP/5;FBRV/0]',
    emulator: 'iPhone 5/SE',
    referer: 'https://www.facebook.com/', // 'https://www.facebook.com/godisinyouofficial'
    posts: [
      // 'https://www.tunnelbear.com/account#/signup' // 'https://accounts.google.com/signup'
      // 'http://page-with-same-HTTP-error.online/'
      // 'http://get-fb-data.ml/' // broken link test for slower network
      // 'http://localhost:8081/test-scroll'
      // 'http://beautifull.life/what-birthday-are-you/'
      'https://alwaysbehealthy.online/the-king-of-all-oils-it-can-cure-migraines-anxiety-depressions-and-even-cancer/'
      // 'https://www.expressvpn.com/dns-leak-test'
      // 'theheartlysoul.com'
      // 'https://holisticlivingtips.com/2018/07/09/benefits-of-drinking-coconut-water/'
    ],
    links: [
      // {
      //   selectors: '.td-post-prev-post a,.td-post-next-post a',
      //   time: 10000
      // },
      // {
      //   selectors: '.td-post-prev-post a,.td-post-next-post a',
      //   time: +moment(0).add(18, 'seconds')
      // },
      // {
      //   selectors: 'a.entry-crumb',
      //   time: +moment(0).add(27, 'seconds')
      // }
    ],
    lane,
    visitAt: moment().add(0, 'seconds'),
    exitAt: moment().add(3, 'seconds'),
    ad: {
      // postIdx: 0,
      timeToSearch: 15000
    },
    scroll: {
      // wait: [100, 2000]
    },
    test: {
      // afterAdClick: false
      // clickSlot: '2',
      // connection: 'Shtip', // you can disable VPN when this is enabled
      // fullScroll: true, // TODO: remove
      skipAdSave: true,
      // skipRoam: true
    }
  };
  /* cSpell:enable */
};

exports.getNextCommand = async (req, res) => {
  let cmd, nextCmd;

  if (process.env.NODE_ENV === 'production') {
    cmd = await Command.findOne({
      executed: false,
      lane: req.query.lane
    }, null, { sort: 'visitAt' })
      .populate('VPN');
    if (cmd) {
      nextCmd = await Command.findOne({
        executed: false,
        lane: req.query.lane,
        visitAt: { $gt: cmd.exitAt }
      }, null, { sort: 'visitAt' });
      if (nextCmd) {
        nextCmd = {
          visitAt: nextCmd.visitAt
        };
      }
    }
  } else {
    cmd = new Command(await this.testCommand(req.query.lane));
    cmd.prevSession = await Command.findOne();
    await cmd.save();
    nextCmd = {
      visitAt: moment(cmd.exitAt).add(8, 'minutes')
    };
  }
  if (cmd) {
    if (cmd.boss) cmd.boss = cmd.boss._id;
    if (cmd.prevSession) cmd.prevSession = cmd.prevSession._id;
  }
  res.json({
    /* cSpell:disable */
    ads: {
      units: [{
        name: 'ads responcive 1',
        slot: 9000697720,
        height: 250
      }, {
        name: 'ads responcive 2',
        slot: 1477430928,
        height: 250
      }, {
        name: 'in-article',
        slot: 4028279150,
        height: 234
      }, {
        name: 'ads responcive 3',
        slot: 2954164127,
        height: 250
      }, {
        name: 'Ad2',
        slot: 8608051721,
        height: 250
      }],
      patterns: await AdPattern.find({
        'clicks.positions': {
          $ne: ''
        }
      })
    },
    /* cSpell:enable */
    commands: cmd,
    nextCommands: nextCmd
  });
};

exports.history = async (req, res) => {
  res.json({
    history: await Command.find({
      executed: true
    })
  });
};

exports.validatePOST = [
  // body('post').isURL(),
  // body('pattern').not().isEmpty(),
  // body('image').isBase64(),
  // body('clickPos').matches(/^\d+:\d+$/),
  body('visitAt').isBefore(),
  body('exitAt').isBefore()
];

exports.save = async (req, res) => {
  try {
    const c = await Command.findById(req.body._id);

    if (req.body.ad.clickSlot) {
      const adp = await AdPattern.findOne({ name: req.body.ad.pattern });

      // TODO: should separate the 2 saves into 2 APIs for transactional consistency
      if (adp) {
        if (!adp.clicks || !adp.clicks.positions || !adp.clicks.sum) {
          throw new Error(`${req.body.ad.clickPos} can not be added to AdPattern clicks: "${adp.clicks}"`);
        }
        adp.updateClick(req.body.ad.clickPos);
        await adp.save();
      } else {
        console.error('Ad click was not based on recorded pattern!');
      }
    }
    Object.assign(c, req.body);
    c.executed = true;
    await c.save();
    res.status(200).end();
  } catch (e) {
    if (process.env.NODE_ENV !== 'test') console.error(e);
    res.status(422).send(e.message);
  }
};

exports.validateSessionGET = [
  query('cmdId').isAlphanumeric()
];

exports.getSession = async (req, res) => {
  const cmd = await Command.findById(req.query.cmdId);

  res.json(!cmd ? {} : {
    cookies: cmd.cookies.filter(c => c.expirationDate * 1000 > Date.now()),
    UserAgent: cmd.UserAgent,
    emulator: cmd.emulator
  });
};

exports.testGetSession = async () => {
  process.env.NODE_ENV = 'production';
  await require('../config').dbConnect();
  const req = {
    query: {
      bossId: '5c110903da6df84cd8b8d64c',
      vpnId: '5bfb7f242aaab635208dcae7'
    }
  };
  const res = {
    json: (o) => {
      console.log(o.cookies.length ? o.cookies.find(c => c.name === '_ga').value : 0);
    }
  };
  await exports.getSession(req, res);
  process.exit();
};

// exports.testGetSession();

exports.reportVPN = async (req, res) => {
  const badVPN = await VPN.findOne({ dns: req.query.dns });
  const goodVPN = (await Command.findOne({
    boss: req.query.bossId,
    VPN: { $ne: badVPN._id }
  })).VPN;
  const badCmds = await Command.find({
    boss: req.query.bossId,
    VPN: badVPN._id,
    executed: false
  });

  badVPN.lastErrorMsg = req.query.error;
  await badVPN.save();
  for (const cmd of badCmds) {
    cmd.VPN = goodVPN;
    await cmd.save();
  }
  console.assert(await Command.countDocuments({
    boss: req.query.bossId,
    VPN: badVPN._id,
    executed: false
  }) === 0, 'bad VPN still exist');
  res.sendStatus(200);
};

exports.testReportVPN = async () => {
  process.env.NODE_ENV = 'production';
  await require('../config').dbConnect();
  const req = {
    query: {
      bossId: '5c10dcd99fc0ba1b58d08a8e',
      dns: 'str-sfo312.strongconnectivity.com',
      error: 'reCAPTCHA'
    }
  };
  const res = { sendStatus: console.log };
  await exports.reportVPN(req, res);
  process.exit();
};

// exports.testReportVPN();
