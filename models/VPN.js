const mongoose = require('mongoose');

module.exports = mongoose.model('VPN', new mongoose.Schema({
  config: Buffer,
  country: String,
  dns: {
    type: String,
    unique: true
  },
  ip: String,
  lastErrorMsg: String,
  owner: String,
  region: String
}, { timestamps: true }));
