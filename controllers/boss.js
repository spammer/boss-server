exports.restrictAccess = (req, cb) => {
  const origin = req.header('Origin');
  let opts = { origin: false }, error = new Error('CORS error!');

  if (~[process.env.ORIGIN || 'http://localhost'].indexOf(origin)) {
    opts = { origin: true, optionsSuccessStatus: 200 };
    error = null;
  }
  cb(error, opts);
};

// disabled because IP can be anything,
// e.g. when inside docker: ::ffff:172.21.0.1
//
// exports.isBot = (req, res, next) => {
//   if (~['::ffff:127.0.0.1', '::1'].indexOf(req.ip)) {
//     return next();
//   }
//   res.sendStatus(403);
// };
