// const fs = require('fs');
// const moment = require('moment');
const mongoose = require('mongoose');

// process.env.NODE_ENV = 'production';
// const { hms } = require('./Time');
const Command = require('../models/Command');
require('../models/VPN');

const analyseCommands = async () => {
  await mongoose.connect('mongodb://localhost:27017/gd-archive', {
    useCreateIndex: true,
    useNewUrlParser: true
  });
  const match = {
    // boss: mongoose.Types.ObjectId('5c192cf4fbb912c8ac26fc55'),
    // IP: '109.248.149.167',
    // _id: {
    //   $nin: [
    //     id('5c0392b5cc3e2e2008ed8ca0'),
    //     id('5c0392b4cc3e2e2008ed80f5'),
    //     id('5c0392b5cc3e2e2008ed8fbe'),
    //     id('5c0392b5cc3e2e2008ed9021')
    //   ]
    // },
    // 'cookies': {
    //   $in: JSON.parse(fs.readFileSync(`${tmpDir}/sessionsAnalytics.json`, 'utf8'))
    // },
    cookies: { $ne: [] },
    prevSessions: { $ne: [] },
    executed: true,
    // sessionDuration: { $lt: 6000 }
    visitAt: {
      $gte: new Date('2018-12-22T00:00:00.000Z'),
      $lte: new Date('2018-12-26T00:00:00.000Z')
    }
  };
  const dbResults = await Command.aggregate([{
    $match: match
  }, {
    $sort: { visitAt: 1 }
  }]);
  const map = {};
  // const tmpDir = `${__dirname}/../tmp`;
  // const t = ms => moment.utc(ms).format('mm:ss');
  // const id = mongoose.Types.ObjectId;
  let sum = 0; // repeated = 0, meta, output = '', sumTime = 0;
  // let o, prevExit, from, to, diff;

  await Command.populate(dbResults, { path: 'VPN' });
  for (const c of dbResults) {
    // const prevSessions = await Command.find({ VPN: c.VPN, exitAt: { $lt: c.visitAt } }, null, { sort: 'visitAt' });
    // if (prevSessions.length) sum++;
    const key = c.cookies.find(o => o.name === '_ga').value;
    map[key] = map[key] + 1 || 1;
    // output += `${c._id} expected duration: ${t(c.exitAt - c.visitAt)}, real duration: ${t(c.sessionDuration)}, start: ${c.sessionStartedAt}\n`;
    // output += `${c.posts[0]}\n`;
    // // output += `${c.errorMsg}\n`;
    // output += `${JSON.stringify(c.links.map((l) => {
    //   l.time = t(l.time);
    //   return l;
    // }), null, 2)}\n\n`;
    // o = {
    //   visitAt: c.visitAt,
    //   exitAt: c.exitAt
    // };
    // map[c.IP] = map[c.IP] ? map[c.IP].concat(o) : [o];
    sum++;
  }
  const mapValues = Object.values(map);
  const newUsers = mapValues.filter(v => v === 1).length;
  const returningUsers = mapValues.filter(v => v > 1).length; // .reduce((a, c) => a + c, 0);
  console.log('new users:', newUsers);
  console.log('returning users:', returningUsers);
  console.log(Math.round((returningUsers / newUsers) * 100), '%');
  // Object.entries(map).sort((a, b) => b[1] - a[1]).forEach(([owner, freq]) => {
  //   console.log(owner, freq);
  //   sum += freq;
  // });
  console.log('total commands:', sum, dbResults.length);
  // fs.writeFileSync(`${tmpDir}/analyseCommands`, output);
  process.exit();
  // Object.entries(map).sort((a, b) => b[1].length - a[1].length).forEach(([ip, sessions]) => {
  //   prevExit = sessions[0].exitAt;
  //   if (sessions.length > 1) repeated++;
  //   sessions = sessions.map((s, i) => {
  //     from = i ? hms(s.visitAt - prevExit) : s.visitAt.toISOString();
  //     diff = s.exitAt - s.visitAt;
  //     to = hms(s.exitAt - s.visitAt);
  //     sumTime += diff;
  //     prevExit = s.exitAt;
  //     return `${from} -> ${to}`;
  //   });
  //   output += `${ip} length=${sessions.length} ${JSON.stringify(sessions, null, 2)}\n`;
  // });
  // meta = `total = ${sum}\nrepeated = ${repeated}\n`;
  // meta += `average session time = ${hms(sumTime / sum)}\n\n`;
  // fs.writeFileSync(`${tmpDir}/analyseCommands`, meta + output);
  // process.exit();
};

analyseCommands(); // '2018-10-04T13:30:00.000Z'
