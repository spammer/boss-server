/* global db, print, tojson */
/* eslint no-restricted-globals: ["error", "event"] */
const c = db.getCollection('ips').find({
  $and: [{
    'vpn.owner': 'IPVanish'
  }, {
    'quality.ipqualityscore.fraud_score': {
      $gte: 76
    }
  }]
});

const result = [];
let i = 0;

while (c.hasNext()) {
  const o = c.next();

  // o._id = o._id.str;
  // delete o.__v;

  result.push(o);
  ++i;
}

print(tojson(result));
print(`count: ${i}`);
