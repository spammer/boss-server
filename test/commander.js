const fs = require('fs');
const moment = require('moment');
const request = require('supertest');
const t = require('tap');
const AdPattern = require('../models/AdPattern');
const { app } = require('../app');
const commander = require('../controllers/commander');
const Boss = require('../models/Boss');
const Command = require('../models/Command');
const VPN = require('../models/VPN');

const testData = {
  boss: new Boss(),
  VPN: new VPN({ dns: 'str-sfo102.strongconnectivity.com', ip: '123.123.13.13' }),
  cookies: [{
    name: 'cookie-name',
    value: 'cookie-name-value'
  }, {
    name: 'another_cookie',
    value: 'some-value',
    expirationDate: Math.round(moment().add(24, 'hours') / 1000)
  }],
  executed: true,
  ad: {
    postIdx: 0,
    pattern: 'h250_image_test',
    size: '335x280',
    code: fs.readFileSync('test/uploads/bg.png', 'utf8'),
    image: fs.readFileSync('test/uploads/bg.png').toString('base64'),
    clickPos: '12:2',
    clickSlot: '9545454',
    advertiser: 'Tester'
  },
  visitAt: moment().subtract(5, 'minutes'),
  exitAt: moment().subtract(10, 'seconds')
};
const originalClicks = {
  positions: '145:1,3:77,212:212,268:100,12:2,77:66',
  sum: 6,
  future: '145:1,12:2,3:77,212:212',
  past: '77:66,268:100'
};
let postData;

const run = async () => {
  postData = Object.assign(await commander.testCommand(1), testData);

  const linkedAP = new AdPattern({
    name: postData.ad.pattern,
    advertiser: postData.ad.advertiser,
    adSize: postData.ad.size,
    image: postData.ad.image,
    clicks: originalClicks
  });
  const c = new Command(postData);

  await Boss.deleteMany();
  await VPN.deleteMany();
  await Command.deleteMany();
  await AdPattern.deleteMany();
  await linkedAP.save();
  await c.save();
  await postData.boss.save();
  await postData.VPN.save();
  postData._id = c._id;

  await t.test('refuse creating history when linked AdPattern has empty clicks.positions', async (t) => {
    const adp = await AdPattern.findOne({ name: postData.ad.pattern });

    adp.clicks = { sum: 10 };
    await adp.save();
    await t.test('subtest', (t) => {
      request(app)
        .post('/api/boss/history')
        .set('Origin', 'http://localhost')
        .send(postData)
        .expect(422)
        .then(async (res) => {
          t.ok(/can not be added to AdPattern clicks/.test(res.text));
          adp.clicks = originalClicks;
          await adp.save();
        })
        .catch(t.fail)
        .finally(t.end);
    });
  });

  await t.test('refuse creating history when clickPos does not exist in linked AdPattern.clicks.positions', (t) => {
    const oldClickPos = postData.ad.clickPos;

    postData.ad.clickPos = '13:13';
    request(app)
      .post('/api/boss/history')
      .set('Origin', 'http://localhost')
      .send(postData)
      .expect(422)
      .then((res) => {
        t.ok(/is not contained in AdPattern clicks.positions/.test(res.text));
        postData.ad.clickPos = oldClickPos;
      })
      .catch(t.fail)
      .finally(t.end);
  });

  await t.test('refuse creating history with invalid cookies', (t) => {
    const oldCookies = postData.cookies.slice().map(o => Object.assign({}, o));

    postData.cookies = JSON.stringify(postData.cookies);
    postData.ad.clickSlot = '';
    request(app)
      .post('/api/boss/history')
      .set('Origin', 'http://localhost')
      .send(postData)
      .expect(422)
      .then((res) => {
        t.ok(/Cast to Array failed for value .+ path "cookies"/.test(res.text));
        postData.cookies = oldCookies;
        postData.ad.clickSlot = testData.ad.clickSlot;
      })
      .catch(t.fail)
      .finally(t.end);
  });

  await t.test('refuse updating click object when given click position does not exist', async (t) => {
    const adp = await AdPattern.findOne({ name: postData.ad.pattern });

    t.throws(() => {
      adp.updateClick('12:3');
    });
  });

  await t.test('update clicks object in AdPattern module', async (t) => {
    const adp = await AdPattern.findOne({ name: postData.ad.pattern });

    adp.updateClick('12:2');
    await adp.save();
    t.equal(adp.clicks.future, '145:1,3:77,212:212');
    t.equal(adp.clicks.past, '77:66,268:100,12:2');

    adp.clicks = originalClicks;
    await adp.save();
  });

  await t.test('update clicks object when empty future and past clicks', async (t) => {
    const adp = await AdPattern.findOne({ name: postData.ad.pattern });

    adp.clicks.future = '';
    adp.clicks.past = '';
    adp.updateClick('12:2');
    await adp.save();
    t.equal(adp.clicks.future, '145:1,3:77,212:212,268:100,77:66');
    t.equal(adp.clicks.past, '12:2');

    adp.clicks = originalClicks;
    await adp.save();
  });

  await t.test('create history record', (t) => {
    request(app)
      .post('/api/boss/history')
      .set('Origin', 'http://localhost')
      .send(postData)
      .expect(200)
      .then(async () => {
        const h = await Command.findById(postData._id).populate('VPN');

        t.ok(h.executed);
        t.ok(h.exitAt.getTime() < moment().valueOf());
        t.equal(h.VPN.ip, testData.VPN.ip);
      })
      .catch(t.fail)
      .finally(t.end);
  });

  await t.test('get cookies on second call', (t) => {
    request(app)
      .get('/api/boss/session')
      .set('Origin', 'http://localhost')
      .query({ cmdId: postData._id.toString() })
      .expect(200)
      .then((res) => {
        const { cookies } = res.body;

        t.equal(cookies.length, 1);
        t.equal(cookies[0].name, postData.cookies[1].name);
        t.ok(cookies[0].expirationDate * 1000 > moment().add(1, 'hour'));
      })
      .catch(t.fail)
      .finally(t.end);
  });

  t.tearDown(process.exit);
};

run();
