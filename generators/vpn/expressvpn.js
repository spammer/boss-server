const { fromPairs } = require('lodash');
const countries = require('../../data/countries');
const { includedCountries, run } = require('../vpn-configs');

// Grab chunk of ovpns from ExpressVPN account page. Run this on every 15m by increasing the slice parameters:
// Array.from(document.querySelectorAll('.region-server a')).slice(0, 20).forEach(a => window.open(a.href))

const cMap = fromPairs(Object.entries(countries)
  .filter(([code]) => ~includedCountries.indexOf(code))
  .map(([code, country]) => [country
    .replace(' ', '_').toLowerCase()
    .replace('united_kingdom', 'uk')
    .replace('united_states', 'usa'), code]));

const filteredCountries = Object.keys(cMap)
  .map(country => `my_expressvpn_${country}_`).join('|');
const getRegionFn = (ovpn, config) => {
  const sections = /^my_expressvpn_(.+)_udp\.ovpn$/.exec(ovpn)[1].split('_-_');
  return {
    serverDNS: /remote ([^\s]+)/gm.exec(config)[1],
    region: `${cMap[sections.shift()]} ${sections.join('-')}`.trim()
  };
};

run('ExpressVPN', filteredCountries, getRegionFn);
