// process.env.NODE_ENV = 'production';
const _ = require('lodash');
const { dbConnect } = require('../config');
const fs = require('fs');
const { hms } = require('./Time');

const analyseStats = async (since, until) => {
  await dbConnect();
  const a = require('../controllers/stats').statsFilterAggregate(since.valueOf(), until.valueOf());
  const dbResults = await require('../models/Stat').aggregate(a);
  const m = {};
  let sum = 0, output = '';

  dbResults.forEach((stat) => {
    let sessions = [];
    const dupSessions = _.countBy(stat.senders.map(s => s.sessionId));

    if (!_.find(dupSessions, v => v > 2)) return;
    stat.senders.forEach((sender) => {
      if (!sender.sessionId) console.error('no session ID for', stat.ip, sender.date);
      if (sender.date < since || sender.date > until) {
        console.error('This should not happen! This is some mongo/mongoose bug?');
        return;
      }
      m[sender.sessionId] = m[sender.sessionId] || [];
      if (m[sender.sessionId].indexOf(stat.ip) === -1) m[sender.sessionId].push(stat.ip);
      sessions.push({
        sessionId: sender.sessionId,
        door: sender.door || sender.clickSlot,
        date: sender.date,
        model: _.get(sender, 'UserDetected.device.model') || _.get(sender, 'UserDetected.os.name'),
        os: sender.UserDetected.os.version
      });
    });

    let prevDate = sessions[0].date, diff;
    sessions = sessions.map((s, i) => {
      if (i > 0) diff = hms(s.date - prevDate);
      else diff = s.date.toISOString();
      prevDate = s.date;
      return `${s.os} ${s.model} ${s.sessionId} (${s.door}) ${diff}`;
    });
    sum += sessions.length;
    output += `${stat.ip} ${JSON.stringify(sessions, null, 2)}\n`;
  });
  for (const s in m) {
    // shouldn't log this strange case
    if (m[s].length > 1) console.error(`Same ${s} is used by multiple IP:`, m[s]);
  }
  output = `total = ${sum}\n\n${output}`;
  fs.writeFileSync(`${__dirname}/../tmp/analyseStats`, output);
  process.exit();
};

analyseStats(new Date('2018-08-05'), new Date('2018-08-08')); // '2018-10-04T13:30:00.000Z'

// discovered possible bots from analysis:
// ------------------------
// 31.13.114.50
// 71.199.218.195
// 69.171.240.19 (facebook)
// 96.89.191.57
