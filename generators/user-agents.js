const _ = require('lodash');
// const chalk = require('chalk');
const Stat = require('../models/Stat');
const UAmap = require('../models/UAmap');
const statsController = require('../controllers/stats');

exports.deviceAggregate = [{
  $unwind: '$senders'
}, statsController.crawlersMatch, {
  $project: {
    _id: 0,
    model: '$senders.UserDetected.device.model',
    UserAgent: '$senders.UserAgent'
  }
}, {
  $match: {
    model: {
      $nin: [
        null,
        'BLADE A110',
        '306G',
        'M200', // Samsung M200 128x160
        'M430'
      ]
    }
  }
}, {
  $group: {
    _id: '$model',
    count: {
      $sum: 1
    },
    UserAgents: {
      $push: '$UserAgent'
    }
  }
}, {
  $sort: {
    count: -1
  }
}];

exports.getEmulator = (UAmap, d) => {
  let emulator;

  _.forEach(UAmap, (similarModels, em) => {
    _.forEach(similarModels, (m) => {
      if ((typeof m === 'string' && d._id === m) || (m instanceof RegExp && m.test(d._id))) {
        emulator = em;
        return false;
      }
    });
    if (emulator) {
      return false;
    }
  });
  if (!emulator) {
    // console.log(`${chalk.cyan(d._id)} is not mapped`);
    // console.log(d); // TODO: clasify missing from production DB
    return false;
  }
  return emulator;
};

exports.disperseUnknownUAs = (UAmap, docs) => {
  UAmap._disperse.forEach((disperse) => {
    const doc = _.find(docs, ['_id', disperse.emulator]);

    _.remove(docs, doc);
    _.forEach(disperse.patterns, (p) => {
      const pUA = [];
      const unwrapped = docs.filter(d => p.models.test(d._id));
      let i = 0;

      doc.UserAgents = doc.UserAgents.filter((ua) => {
        const ret = p.ua.test(ua);

        if (ret) {
          pUA.push(ua);
        }
        return !ret;
      });
      pUA.forEach((ua) => {
        const d2 = unwrapped[i++ % unwrapped.length];

        d2.UserAgents.push(ua);
        d2.count++;
      });
    });
  });
  delete UAmap._custom;
};

exports.generateUAs = async (wantedSessions = 300, thresholdPercent = 0.05) => {
  const threshold = Math.ceil(thresholdPercent * wantedSessions);
  const filteredDocs = [];
  const generatedUA = [];
  let leftSessions = wantedSessions;

  const docs = await Stat.aggregate(this.deviceAggregate);
  let totalUA = 0;

  this.disperseUnknownUAs(UAmap, docs);
  _.forEach(docs, (d) => {
    const counts = {};

    d.emulator = this.getEmulator(UAmap, d);
    if (~UAmap._whitelist.indexOf(d.emulator)) {
      d.UserAgents.forEach((ua) => {
        counts[ua] = (counts[ua] || 0) + 1;
      });
      totalUA += d.UserAgents.length;
      d.groupedUserAgents = _.map(counts, (c, ua) => [ua, c, c / d.UserAgents.length])
        .sort((a, b) => b[1] - a[1]);
      filteredDocs.push(d);
    }
  });
  _.forEach(filteredDocs, (d, i) => {
    const totalSubsessions = Math.ceil(wantedSessions * (d.count / totalUA));
    let leftSubsessions = totalSubsessions;

    if (leftSessions < threshold) {
      const slicedDocs = docs.slice(i);

      for (let j = generatedUA.length; j < wantedSessions; j++) {
        const randomDoc = _.sample(slicedDocs);

        generatedUA.push({
          emulator: randomDoc.emulator,
          UAString: _.sample(randomDoc.UserAgents)
        });
      }
      return false;
    }
    _.forEach(d.groupedUserAgents, (a) => {
      const duplicateUAs = Math.ceil(totalSubsessions * a[2]);

      leftSubsessions -= duplicateUAs;
      if (leftSubsessions < 0) {
        return false;
      }
      generatedUA.push(...Array(duplicateUAs).fill({
        emulator: d.emulator,
        UAString: a[0]
      }));
    });
    leftSessions -= totalSubsessions;
  });

  return _.shuffle(generatedUA);
};

// (async () => {
//   process.env.NODE_ENV = 'production';
//   await require('../config').dbConnect();
//   const uas = await exports.generateUAs();
//   console.log('results', uas.length);
//   console.log(uas[0]);
//   process.exit();
// })();
